var client;
var mqttConnectionTimeout;
var randNumber = Math.floor(Math.random() * 1000)
var clientId = "Depo-Kuyu-Web" + randNumber
var username ="DepoKuyuWeb";
var password = "depokuyuweb";
var webToServerDestination = "DepoKuyu/Web";
var serverToWebDestination = "DepoKuyu/Server";
var sendedMessageDestination = []; //Gönderilen komut konusunun split edilerek tutan dizi değişkeni
var sendedCommand; //Gönderilen komut kodunu tutan değişkendir.Gelen cevapla karşılaştırma yapmak için
var sendedCommandType; //Gömderilen komut türünü tutan değişken. Komut türüne cevap döndükten sonra hem cevabı kontrol eder hemde işlemi belirler
var waitResponse = false;


function loadingON() {
    $("#loading-modal").modal("show");
}

function loadingOFF() {
    $("#loading-modal").modal('hide');
}

function errormodalON() { //MQTT ile bağlantı kurulmadığında error modalı aktif olur
    $('#error-connection-modal').modal('show')
}

function errormodalOFF() {
    $('#error-connection-modal').modal('hide')
}

function mqttConnect(ip, port) {
    loadingON();
    mqttConnectionTimeout = setTimeout(function () {
        console.log("open error modal")
        errormodalON()
        loadingOFF()
    }, 6000)

    client = new Paho.MQTT.Client(ip, port, clientId);
    
        client.connect({
            onSuccess: onConnectHome,
        });
        client.onConnectionLost = onConnectionLost;
        client.onMessageArrived = onMessageArrived;
}
function onConnectHome() {
    setTimeout(loadingOFF, 1000)
    console.log("onConnect");
    clearTimeout(mqttConnectionTimeout);
    // systemSubscribe();
    $.ajax({
        url: "./home/getserialsformqtt",
        type: "GET",
        success: function (data) {
            var devicecount = data.length;
            $.each(data, function (index, item) {
                clientSubscribe(item.serial)
            }) 
        },
        error: function (data) {
            console.log(data)
        }
    })
}
function onConnectSettings(){
    setTimeout(loadingOFF,500)
    console.log("onConnect");
    clearTimeout(mqttConnectionTimeout);
    systemSubscribe();
}
function clientSubscribe(serial) {
    client.subscribe(serial + "/DKW/RES");
    console.log("Subscribed -> " + serial);  
}
function systemSubscribe(){
    client.subscribe(serverToWebDestination)
    console.log("Subscribed-> "+serverToWebDestination)
}
function clientSend(message, commandtype, command) {
    client.send(message);
    sendedCommandType = commandtype;
    sendedCommand = command;
    sendedMessageDestination = message.destinationName.split("/");
    waitResponse = true;
    console.log("------------------Giden Mesaj----------------------")
    console.log("Konu ;")
    console.log(message.destinationName);
    console.log("Byte Türü ;")
    console.log(message.payloadBytes);
    var messagestring = new TextDecoder("utf-8").decode(message.payloadBytes);
    console.log("String Türü ;");
    console.log(messagestring);
    var nowDate = new Date();
    console.log("Tarih Saat ;");
    console.log(nowDate.toLocaleString());
    console.log("---------------------------------------------------")
}
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
        client.connect({
            onSuccess: onConnect
        });
    }
}
function onMessageArrived(message) {
    console.log("------------------Gelen Mesaj--------------------:");
    console.log("Konu ;")
    console.log(message.destinationName);
    console.log("Byte Türü ;")
    console.log(message.payloadBytes);
    var messagestring = new TextDecoder("utf-8").decode(message.payloadBytes);
    console.log("String Türü ;");
    console.log(messagestring);
    var nowDate = new Date();
    console.log("Tarih Saat ;");
    console.log(nowDate.toLocaleString());
    console.log("---------------------------------------------------");

    if(messagestring.indexOf("seviye")>=0){
        var seviye = messagestring.split("-")[1];
        waterTank.waterTank(Number(seviye));
    }

    var arrivedMessageDestiantion = message.destinationName.split("/");
    var arrivedCommand = message.payloadBytes
    console.log({arrivedCommand,sendedCommand})
    if(arrivedCommand==sendedCommand){
        waitResponse= false;
        if(sendedCommand==4){
            console.log("geldi :)")
        }

    }else if(sendedCommandType ==randNumber){
        if(sendedCommand =="SettingsUpdated"){
            console.log("SettingsUpdated")
                waitResponse= false;
        }else if(sendedCommand == "NewDepoAdded"){
            console.log("NewDepoAdded")
            waitResponse = false;
        }else if(sendedCommand == "NewKuyuAdded"){
            console.log("NewKuyuAdded")
            waitResponse= false;
        }else if(sendedCommand== "DeleteDepo"){
            console.log("DeletedDepo")
            waitResponse= false;
        }else if(sendedCommand== "DeleteKuyu"){
            console.log("DeletedKuyu")
            waitResponse= false;
        }else if(sendedCommand == "devicesSettingsUpdated"){
            console.log("Cihaz Ayarları Güncellendi");
            waitResponse= false;
        }
    }
}



