@extends('layouts.app',['page'=>'home'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container ">Kontrol Merkezi</h3>
        <hr style="border-color: #33333350">
    </div>
</div>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            {{-- Depo Table --}}
            <div class="col-lg-6 px-2">
                <div class="card bg-white" style="border-radius:30px">
                    <div class="card-header">
                        <h3 style="color:#333; margin:0px"><i class="fa fa-assistive-listening-systems"
                                aria-hidden="true" style="color:#555;vertical-align: initial; font-size: 30px;"></i>
                            Depo Cihazları</h3>
                        <button id="btn-open-modal-add-depo" class="btn btn-success btn-sm float-right"
                            onclick="clickAddDepo()">Yeni Oluştur</button>
                        <hr>
                    </div>
                    <div class="card-body">
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="depo_table" class="table table-striped table-no-bordered table-hover text-center"
                                    cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Depo Adı</th>
                                            <th>Seri No</th>
                                            <th>Depo Hacmi</th>
                                            <th>Kuyu Sayısı</th>
                                            <th class="disabled-sorting text-right">İşlemler</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($depos as $depo)
                                        <tr> 
                                            <td>
                                                <a class="btn btn-link m-0 p-0" style="width:100%"
                                                    onclick="loadkuyu({{$depo->id}},{{$depo->serial}},'{{$depo->name}}')">
                                                    @if($depo->connected)
                                                    <i class="fa fa-circle" style="color: green"></i>
                                                    @else
                                                    <i class="fa fa-circle" style="color: red"></i>
                                                    @endif{{$depo->name}}
                                                </a>
                                            </td>
                                            <td>{{$depo->serial}}</td>
                                            <td>{{$depo->depo_volume}}</td>
                                            <td>{{$depo->childsize}}</td>
                                            <td class='text-right'>
                                                <div style="display:inline-flex">
                                                    <button class="btn btn-link btn-warning btn-just-icon"onclick="goToDepoDetail({{$depo->id}},'{{$depo->serial}}','{{$depo->name}}',{{$depo->depo_height}},{{$depo->depo_volume}},{{$depo->depo_max_level}},{{$depo->depo_min_level}},'{{$depo->baslangic_saati}}','{{$depo->bitis_saati}}','{{$depo->control_time}}')"><i class="fa fa-hourglass-half" aria-hidden="true"></i></button>
                                                      {{-- @if (Auth::user()->role_id=="1" ||Auth::user()->role_id=="2") --}}
                                                    {{-- <button class="btn btn-link btn-warning btn-just-icon" onclick="openDepoSettingsModal({{$depo->id}},'{{$depo->serial}}','{{$depo->name}}',{{$depo->height}},{{$depo->depo_volume}},{{$depo->depo_max_level}},{{$depo->depo_min_level}})"><i class="fa fa-cog" aria-hidden="true"></i></button> --}}
                                                    <button class="btn btn-link btn-success btn-just-icon" onclick="opendepoeditmodal({{$depo->id}})"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                    {{-- @endif --}}
                                                </div>
                                            </td>    
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            {{-- Kuyu Table --}}
            <div class="col-lg-6 px-2">
                <div class="card bg-white" style="border-radius:30px">
                    <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <h3 style="color:#333; margin:0px"><i class="fa fa-assistive-listening-systems"
                                            aria-hidden="true" style="color:#555;vertical-align: initial; font-size: 30px;"></i>
                                        Kuyu Cihazları</h3>
                                </div>
                                <div class="col-6 text-right">
                                    <h5 id="kuyu-card-title" class="text-dark pt-6 m-0"></h5>
                                </div>
                            </div>  
                        <button id="btn-open-modal-add-kuyu" class="btn btn-success btn-sm float-right"
                            onclick="clickAddKuyu()">Yeni Oluştur</button>
                        <hr>
                    </div>
                    <div class="card-body">
                        <div class="material-datatables">
                            <div class="table-responsive">
                                <table id="kuyu_table" class="table table-striped table-no-bordered table-hover text-center"
                                    cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Kuyu Adı</th>
                                            <th>Seri no</th>
                                            <th>Pompa Debisi</th>
                                            <th>Depo Sayısı</th>
                                            <th>Toplam Çalışma Saati</th>
                                            <th class="disabled-sorting text-right">İşlemler</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"  id="depo_detail">
            <div class="col-6">
                <div class="tank waterTankHere1"></div>
            </div>
        </div>
    </div>
</div>
{{-- Kuyu Control modalı --}}
<div class="modal fade" id="control-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static" data-keyboard="false" style="background-color:rgba(0,0,0,0.2)">
    <div class="modal-dialog" style="margin-top: 2rem; max-width: 500px" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="text-right" style="margin:-20px -10px">
                    <button class='btn btn-link btn-just-icon' data-dismiss="modal"><i class='fa fa-close'></i></button>
                </div>
                <h3 class="text-dark mb-2">Kuyu Cihaz Kontrolü</h3>
                <h5 class="text-dark mb-2 modal-title"></h5>
                <div class="card mb-1">
                    <div class="card-body">
                        <div class="row py-2">
                            <div class="col-6">
                                <button type="button" id="btn-role1-open" onclick="ctrlRole('on')"
                                    class="btn btn-fill btn-success">Kuyu Aç</button>
                            </div>
                            <div class="col-6">
                                <button type="button" id="btn-role2-open" onclick="ctrlRole('off')"
                                    class="btn btn-fill btn-danger">Kuyu Kapat</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Depo Cihaz Kontrolü --}} 
<div class="modal fade" id="add-depo-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static" data-keyboard="false" style="background-color:rgba(0,0,0,0.2)">
    <div class="modal-dialog" style="margin-top: 2rem; max-width: 840px " role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="text-right" style="margin:-20px -10px">
                    <button class='btn btn-link btn-just-icon' data-dismiss="modal"><i class='fa fa-close'></i></button>
                </div>
                <h3 class="text-dark mb-2">Depo Cihazı Ekle</h3>
                <div class="card mb-1">
                    <div class="card-body depo-modal-scroll" style="overflow-y: scroll; max-height: 75vh;">
                        <form id="addDepoForm">
                                <div class="row">
                                    <div class="col-12 border-success mb-1 d-inline text-center"
                                        style="border-width: 1px; border-style: solid; border-radius: 30px">
                                        <label class="text-dark" style="font-size: 16px"><b>Not:</b> İsteğe bağlı
                                            alanlar
                                            boş bırakılabilir.</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <div class="float-left">
                                            <label>Depo Adı : </label> 
                                        </div>
                                            <input type="text" id="txt_depo_name" name="name" class="form-control mt-2" placeholder="Depo İsmini Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Seri Numarası : </label> 
                                            </div> 
                                            <input type="text" id="txt_depo_seri_no" name="serial" class="form-control mt-2 inputnumber" placeholder="Cihazın Seri Numarasını Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Depo Hacmi : </label> 
                                            </div>
                                            
                                            <input type="text" id="txt_depo_volume" name="depo_volume" class="form-control mt-2 inputnumber" placeholder="Depo Hacmini Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Depo Yüksekliği : </label> 
                                            </div>
                                            <input type="text" id="txt_depo_height" name="depo_height"
                                                class="form-control mt-2 inputnumber" placeholder="Depo Yüksekliğini Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label class="text-dark">Kuyu Seçimi : </label> 
                                            </div>
                                            {{-- <label class=" text-dark" for="deposkuyus">Kuyu Seçimi :</label> --}}
                                            <select class="form-control select2" id="deposkuyusadd" name="deposkuyus[]"
                                                multiple="multiple">
                                                @foreach ($kuyus as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Maximum Seviye: </label> 
                                            </div>
                                            
                                            <input type="text" id="txt_depo_max_levels" name="depo_max_level"
                                                class="form-control mt-2 inputnumber" placeholder="Deponun Maximum Doluluk Oranını Giriniz"  required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Minimum Seviye : </label> 
                                            </div>
                                            
                                            <input type="text" id="txt_depo_min_levels" name="depo_min_level"
                                                class="form-control mt-2 inputnumber" placeholder="Deponun Minimum Doluluk Oranını Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Başlangıç Saati : </label> 
                                            </div>
                                            
                                            <input type="text" id="depo_baslangic_saati" name="depo_baslangic_saati"
                                                class="form-control mt-2 inputnumber" placeholder="Sms İçin Gönderim Başlangıç Saatini Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Bitiş Saati : </label> 
                                            </div>
                                            
                                            <input type="text" id="depo_bitis_saati" name="depo_bitis_saati"
                                                class="form-control mt-2 inputnumber" placeholder="Sms İçin Gönderim Bitiş Saatini Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Kontrol Süresi : </label> 
                                            </div>
                                            
                                            <input type="text" id="depo_control_time" name="depo_control_time"
                                                class="form-control mt-2 inputnumber" placeholder="Cihazın Ulaşılabilirlik Kontrol Süresini Giriniz" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Telefon Numarası1 : </label> 
                                            </div>
                                            
                                            <input type="text" id="depo_phone_1" name="depo_phone_1"
                                                class="form-control mt-2 inputnumber" placeholder="SMS Gidecek 1. Telefon Numarası Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Telefon Numarası2  : <small>*isteğe bağlı</small> </label> 
                                            </div>
                                            
                                            <input type="text" id="depo_phone_2" name="depo_phone_2"
                                                class="form-control mt-2 inputnumber" placeholder="SMS Gidecek 2. Telefon Numarası Giriniz">
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Telefon Numarası3  : <small>*isteğe bağlı</small> </label> 
                                            </div>
                                            
                                            <input type="text" id="depo_phone_3" name="depo_phone_3"
                                                class="form-control mt-2 inputnumber" placeholder="SMS Gidecek 3. Telefon Numarası Giriniz">
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Email1 : </label> 
                                            </div>
                                            
                                            <input type="email" id="depo_email1" name="depo_email1"
                                                class="form-control mt-2 inputnumber" placeholder="Mail Gidecek 1.Email Adresinizi Giriniz">
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Email2 : <small>*isteğe bağlı</small></label> 
                                            </div>
                                            
                                            <input type="email" id="depo_email2" name="depo_email2"
                                                class="form-control mt-2 inputnumber" placeholder="Mail Gidecek 2.Email Adresinizi Giriniz">
                                        </div>
                                    </div>
                                </div>
                            <div class="card-footer">
                                {{ csrf_field() }}
                                <div class="row text-right float-right">
                                    <button type="button" onclick="addDepo()"
                                        class="btn btn-fill btn-warning">Kaydet</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="add-kuyu-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static" data-keyboard="false" style="background-color:rgba(0,0,0,0.2)">
    <div class="modal-dialog" style="margin-top: 2rem; max-width: 840px " role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="text-right" style="margin:-20px -10px">
                    <button class='btn btn-link btn-just-icon' data-dismiss="modal"><i class='fa fa-close'></i></button>
                </div>
                <h3 class="text-dark mb-2">Kuyu Cihazı Ekle</h3>
                <div class="card mb-1">
                    <div class="card-body kuyu-modal-scroll" style="overflow-y: scroll; max-height: 75vh;" >
                        <form>
                            <div class="row">
                                    <div class="col-12 border-success mb-1 d-inline text-center"
                                        style="border-width: 1px; border-style: solid; border-radius: 30px">
                                        <label class="text-dark" style="font-size: 16px"><b>Not:</b> İsteğe bağlı
                                            alanlar
                                            boş bırakılabilir.</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Kuyu Adı : </label> 
                                            </div>
                                            <input type="text" id="txt_kuyu_name" name="name" class="form-control" placeholder="Kuyu Adını Giriniz"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Seri Numarası : </label> 
                                            </div>
                                            <input type="text" id="txt_kuyu_serial" name="serial"
                                                class="form-control inputnumber" required placeholder="Cihazın Seri Numarasını Giriniz">
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Pompa Debi : </label> 
                                            </div>
                                            <input type="text" id="pompa_debi" name="pompa_debi" class="form-control inputnumber"
                                                required placeholder="Pompa Debisini Giriniz">
                                        </div>
                                        {{-- kuyunun çalışma saati sonradan kaldırılabilir --}}
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Çalışma Saati : </label> 
                                            </div>
                                            <input type="text" id="total_working_hours" name="total_working_hours" class="form-control inputnumber"
                                            required placeholder="Kuyu Çalışma Saatini Giriniz">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Başlangıç Saati : </label> 
                                            </div>
                                            <input type="text" id="txt_kuyu_baslangic_saati" name="txt_kuyu_baslangic_saati" class="form-control inputnumber"
                                            placeholder="Sms İçin Gönderim Başlangıç Saatini Giriniz"    required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Bitiş Saati : </label> 
                                            </div>
                                            <input type="text" id="txt_kuyu_bitis_saati" name="txt_kuyu_bitis_saati" class="form-control inputnumber"
                                            placeholder="Sms İçin Gönderim Bitiş Saatini Giriniz"    required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Kontrol Süresi : </label> 
                                            </div>
                                            <input type="text" id="txt_kuyu_control_time" name="txt_kuyu_control_time" class="form-control inputnumber"
                                            placeholder="Cihazın Ulaşılabilirlik Kontrol Süresini Giriniz" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Telefon Numarası1 : </label> 
                                            </div>
                                            <input type="text" id="kuyu_phone_1" name="kuyu_phone_1" class="form-control" placeholder="SMS Gidecek 1. Telefon Numarası Giriniz"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Telefon Numarası2 : <small>*isteğe bağlı</small> </label> 
                                            </div>
                                            <input type="text" id="kuyu_phone_2" name="kuyu_phone_2" class="form-control" placeholder="SMS Gidecek 2. Telefon Numarası Giriniz"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Telefon Numarası3 : <small>*isteğe bağlı</small> </label> 
                                            </div>
                                            <input type="text" id="kuyu_phone_3" name="kuyu_phone_3" class="form-control" placeholder="SMS Gidecek 3. Telefon Numarası Giriniz"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Email1 : </label> 
                                            </div>
                                            <input type="email" id="kuyu_email1" name="kuyu_email1" class="form-control" placeholder="Mail Gidecek 1.Email Adresinizi Giriniz"
                                                required>
                                        </div>
                                        <div class="form-group">
                                            <div class= "float-left">
                                                <label>Email2 : <small>*isteğe bağlı</small></label> 
                                            </div>
                                            <input type="email" id="kuyu_email2" name="kuyu_email2" class="form-control" placeholder="Mail Gidecek 2.Email Adresinizi Giriniz"
                                                required>
                                        </div>
                                    </div>
                                </div>
                            <div class="card-footer">
                                {{ csrf_field() }}
                                <div class="row text-right float-right">
                                    <button type="button" onclick="addKuyu()"
                                        class="btn btn-fill btn-warning">Kaydet</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-depo-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 2rem; max-width: 840px " role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="text-right" style="margin:-20px -20px">
                    <button class='btn btn-link btn-just-icon' data-dismiss="modal"><i class='fa fa-close'></i></button>
                </div>
                <h3 class="text-dark mb-2">Depo Cihazı Düzenle</h3>
                <h5 class="text-dark mb-2 modal-title"></h5>
                <div class="card mb-1">
                    <div class="card-body depo-edit-modal-scroll" style="overflow-y: scroll; max-height: 75vh;">
                        <form id="editDepoForm">
                            <div class="row">
                                    <div class="col-12 border-success mb-1 d-inline text-center"
                                        style="border-width: 1px; border-style: solid; border-radius: 30px">
                                        <label class="text-dark" style="font-size: 16px"><b>Not:</b> İsteğe bağlı
                                            alanlar
                                            boş bırakılabilir.</label>
                                    </div>
                                    <input type="hidden" id="id" name="depoid">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="float-left">
                                            <label>Depo Adı : </label>
                                            </div>
                                            <input type="text" id="fname" name="name" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                            <label>Seri Numarası : </label>
                                            </div>
                                            <input type="text" id="serial" name="serial" class="form-control"
                                                placeholder="Seri No" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                            <label>Depo Hacmi : </label>
                                            </div>
                                            <input type="text" id="depo_volume" name="depo_volume" class="form-control"
                                                placeholder="Depo Hacmi" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                            <label>Depo Yüksekliği : </label>
                                            </div>
                                            <input type="text" id="depo_height" name="depo_height" class="form-control"
                                                placeholder="Depo Yüksekliği" required>
                                        </div>
                                        <div class="form-group " style="text-align: left;">
                                            <label class="text-dark" for="deposkuyus">Kuyu Seçimi :</label>
                                            <select class="form-control select2" id="deposkuyusedit" name="deposkuyus[]"
                                                multiple="multiple">
                                                @foreach ($kuyus as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="float-left">
                                            <label>Maximum Seviye : </label>
                                            </div>
                                            <input type="text" id="depo_max_level" name="depo_max_level" class="form-control"
                                                placeholder="Depo Maximum Seviye" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Minimum Seviye : </label>
                                            </div>
                                            <input type="text" id="depo_min_level" name="depo_min_level" class="form-control"
                                                placeholder="Depo Minmum Seviye" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Kontrol Süresi : </label>
                                            </div>
                                            <input type="text" id="edit_depo_control_time" name="edit_depo_control_time" class="form-control"
                                                placeholder="Kontrol Süresi" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Başlangıç Saati : </label>
                                            </div>
                                            <input type="text" id="edit_baslangic_saati" name="edit_baslangic_saati" class="form-control"
                                                placeholder="Başlangıç Saati" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Bitiş Saati : </label>
                                            </div>
                                            <input type="text" id="edit_bitis_saati" name="edit_bitis_saati" class="form-control"
                                                placeholder="Bitiş Saati" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Telefon Numarası1 : </label>
                                            </div>
                                            <input type="text" id="edit_depo_phone1" name="edit_depo_phone1" class="form-control"
                                                placeholder="Telefon Numarası 1" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Telefon Numarası2 : <small>*isteğe bağlı</small> </label>
                                            </div>
                                            <input type="text" id="edit_depo_phone2" name="edit_depo_phone2" class="form-control"
                                                placeholder="Telefon Numarası 2" >
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Telefon Numarası3 : <small>*isteğe bağlı</small> </label>
                                            </div>
                                            <input type="text" id="edit_depo_phone3" name="edit_depo_phone3" class="form-control"
                                                placeholder="Telefon Numarası 3" >
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Email1 : </label>
                                            </div>
                                            <input type="email" id="edit_depo_email1" name="edit_depo_email1" class="form-control"
                                                placeholder="1.Email Adresi">
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Email2 : <small>*isteğe bağlı</small></label>
                                            </div>
                                            <input type="email" id="edit_depo_email2" name="edit_depo_email2" class="form-control"
                                                placeholder="2.Email Adresi">
                                        </div>
                                    </div>
                            </div>
                            <div class="card-footer">
                                {{ csrf_field() }}
                                <div class="row text-right float-right">
                                    <button type="button" class="btn btn-fill btn-danger" onclick="deletedepo()">Sil</button>
                                </div>
                                <div class="row text-right float-right">
                                    <button type="submit" class="btn btn-fill btn-success">Kaydet</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-kuyu-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document" style="margin-top: 2rem; max-width: 840px " role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="text-right" style="margin:-20px -10px">
                    <button class='btn btn-link btn-just-icon' data-dismiss="modal"><i class='fa fa-close'></i></button>
                </div>
                <h3 class="text-dark mb-2">Kuyu Cihazı Düzenle</h3>
                <div class="card mb-1">
                    <div class="card-body kuyu-edit-modal-scroll" style="overflow-y: scroll; max-height: 75vh;">
                        <form id="editKuyuForm">
                            <div class="row">
                                <div class="col-12 border-success mb-1 d-inline text-center"
                                style="border-width: 1px; border-style: solid; border-radius: 30px">
                                <label class="text-dark" style="font-size: 16px"><b>Not:</b> İsteğe bağlı
                                    alanlar
                                    boş bırakılabilir.</label>
                                </div>
                                <input type="hidden" id="id" name="kuyuid">
                                {{-- <div class="col-12 text-dark"> --}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="float-left">
                                            <label>Kuyu Adı :</label>
                                            </div>
                                            <input type="text" id="kname" name="name" class="form-control"
                                                placeholder="Kuyu Adı" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Seri Numarası :</label>
                                            </div>
                                            <input type="text" id="edit_kuyu_serial" name="serial" class="form-control"
                                                placeholder="Seri No" required readonly>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Pompa Debisi :</label>
                                            </div>
                                            <input type="text" id="edit_pompa_debi" name="pompa_debi" class="form-control"
                                                placeholder="Pompa Debisi" required>
                                        </div>
                                        <div class="form-group" style="text-align: left;">
                                            <label class="text-dark" for="kuyuselect">Depo Seçimi :</label>
                                            <select class="form-control select2" id="kuyusdeposedit" name="kuyusdepos[]"
                                                multiple="multiple">
                                                @foreach($depos as $item)
                                                <option value="{{$item->id}}">{{$item->name}} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4" >   
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Toplam Çalışma Saati :</label>
                                            </div>
                                            <input type="text" id="edit_total_working_hours" name="total_working_hours" class="form-control"
                                                placeholder="Toplam Çalışma Saati" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Kontrol Süresi :</label>
                                            </div>
                                            <input type="text" id="kuyu_edit_control_time" name="kuyu_edit_control_time" class="form-control"
                                                placeholder="Kontrol Süresi" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Başlangıç Saati :</label>
                                            </div>
                                            <input type="text" id="kuyu_edit_baslangic_saati" name="kuyu_edit_baslangic_saati" class="form-control"
                                                placeholder="Başlangıç Saati" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Bitiş Saati:</label>
                                            </div>
                                            <input type="text" id="kuyu_edit_bitis_saati" name="kuyu_edit_bitis_saati" class="form-control"
                                                placeholder="Bitis Saati" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Telefon Numarası 1 :</label>
                                            </div>
                                            <input type="text" id="edit_kuyu_phone1" name="edit_kuyu_phone1" class="form-control"
                                                placeholder="Telefon Numarası 1" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Telefon Numarası 2 : <small>*isteğe bağlı</small></label>
                                            </div>
                                            <input type="text" id="edit_kuyu_phone2" name="edit_kuyu_phone2" class="form-control"
                                                placeholder="Telefon Numarası 2" >
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Telefon Numarası 3 : <small>*isteğe bağlı</small></label>
                                            </div>
                                            <input type="text" id="edit_kuyu_phone3" name="edit_kuyu_phone3" class="form-control"
                                                placeholder="Telefon Numarası 3">
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Email1 :</label>
                                            </div>
                                            <input type="email" id="edit_kuyu_email1" name="edit_kuyu_email1" class="form-control"
                                                placeholder="1.Email Adresini Giriniz" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="float-left">
                                                <label>Email2 : <small>*isteğe bağlı</small></label>
                                            </div>
                                            <input type="email" id="edit_kuyu_email2" name="edit_kuyu_email2" class="form-control"
                                                placeholder="2.Email Adresini Giriniz" >
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="card-footer">
                                {{ csrf_field() }}
                                <div class="row text-right float-right">
                                    <button type="button" class="btn btn-fill btn-danger" onclick="deletekuyu()">Sil</a>
                                </div>
                                <div class="row text-right float-right">
                                    <button type="submit" class="btn btn-fill btn-success">Kaydet</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error-connection-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="margin-top: 100px">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-light">
            <div class="modal-body text-center">
                <h3 class="text-dark mb-2">İnternet bağlantınızı kontrol edin!</h3>
                <button type="button" class="btn btn-secondary" onclick="refreshPage()">Tekrar Dene</button><br>
                <button type="button" class="btn btn-sm btn-danger" onclick="errormodalOFF()">Devam Et</button><br>
                <span>Lütfen internet bağlantınızı ve cihaz ayarlarını kontrol edin.</span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="loading-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: rgba(0, 29, 57,0.95);border-radius:40px;">
            <div class="modal-body text-center" style="height:30em; padding-left:6em; padding-top:9em">
                <div id="loading bg-transparent" style="">
                    <div id="preloader">
                        <div id="loader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/js/waterTank.js')}}"></script>
<script>
    var mqttValues = {
        ip: "{{$settings->mqtt_ip}}",
        port: "{{$settings->mqtt_port}}",
    }
    var depotable = null; //Sayfada yer alan depo tablosunu tutar
    var kuyutable = null; //Sayfada yer alan kuyu tablosunu tutar
    var depoid = 0;
    var deposerial = 0; //mqtt yayını için konu adında kullanılır
    var deponame = "";
    var depochildsize = 0;
    var depo_height = 0;
    var depo_volume = 0;
    var depo_max_level = 0;
    var depo_min_level = 0;
    var kuyuid = 0;
    var kuyuserial = "";
    var kuyuname = "";
    var kuyuchildsize = 0;
    var waterTank = null;
    
    $(document).ready(function () {
        depotable = $('#depo_table').DataTable({
            reponsive: true,
            "pagingType": "numbers",
            "order": [1, "asc"],
            "lengthMenu": [
                [5, 10, -1],
                [5, 10, "All"]
            ],
            language: {
                search: "Search",
                searchPlaceholder: "Search records",
                "info": "",
            }
        });
        kuyutable=$('#kuyu_table').DataTable({
            reponsive: true,
            "pagingType": "numbers",
            "order": [1, "asc"],
            "lengthMenu": [
                [5, 10, -1],
                [5, 10, "All"]
            ],
            "language": {
                search: "Search",
                searchPlaceholder: "Search records",

            }
        });
        $('#deposkuyusadd').select2({
            width: '100%'
        });
        $('#deposkuyusedit').select2({
            width: '100%'
        }).val();
        $('#kuyusdeposedit').select2({
            width: '100%'
        });
        mqttConnect(mqttValues.ip, Number(mqttValues.port), "home");
        waterTank=$('.waterTankHere1').waterTank({
            width: 420,
            height: 360,
            color: 'skyblue',
            level: 83
        })
    });

    function KuyuKontrolModal(serial) {
        console.log(serial)
        kuyuserial = serial
        $("#control-modal").modal("show");
    }
    function goToDepoDetail(id,serial,name,depo_height,depo_volume,depo_max_level,depo_min_level,baslangic_saati,bitis_saati,control_time) {
        getdeposeviye(serial);
        depoid=id;
        deposerial = serial;
        depo_height = depo_height;
        depo_volume = depo_volume;
        depo_max_level = depo_max_level;
        depo_min_level = depo_max_level;
        baslangic_saati = baslangic_saati;
        bitis_saati= bitis_saati;
        control_time= control_time;
        document.getElementById('depo_detail').scrollIntoView();
    }
    function getdeposeviye(serial){
        send("seviye",serial)
    }
    function ctrlRole(cmd) { //Kuyu cihazıını manuel olarak açma işlemi
        send(cmd, kuyuserial)
    }
    function send(str, serial) {
        console.log(serial + "-CMD -> " + str)
        message = new Paho.MQTT.Message(str);
        message.destinationName = serial + "/DKW/CMD"; // Webden cihaza veri gönderme
        client.send(message);
    }
    //Yeni depo ekleme fonksiyonu
    function addDepo() {
        var depo_name = $("#txt_depo_name").val()
        var serial = $("#txt_depo_seri_no").val()
        var depo_volume = $("#txt_depo_volume").val()
        var depo_height = $("#txt_depo_height").val()
        var depo_max_level = $("#txt_depo_max_levels").val()
        var depo_min_level = $("#txt_depo_min_levels").val()
        var baslangic_saati = $("#depo_baslangic_saati").val()
        var bitis_saati = $("#depo_bitis_saati").val()
        var control_time = $("#depo_control_time").val()
        var phone1 = $("#depo_phone_1").val()
        var phone2 = $("#depo_phone_2").val()
        var phone3 = $("#depo_phone_3").val()
        var email1 = $("#depo_email1").val()
        var email2 = $("#depo_email2").val()
        var selectdepo = $("#deposkuyusadd").val()
        //select gelecekmi
        $.ajax({
            url: "./depo/store?name=" + depo_name + "&serial=" + serial + "&depo_volume=" + depo_volume +
                "&depo_height=" + depo_height + "&depo_max_level=" + depo_max_level + "&depo_min_level=" +
                depo_min_level + "&baslangic_saati=" +baslangic_saati+ "&bitis_saati=" +bitis_saati+ "&control_time=" +control_time+ "&phone1="+phone1+ "&phone2=" +phone2+ "&phone3=" +phone3+ "&deposkuyus=" +selectdepo+ "&email1=" +email1+ "&email2=" +email2,
            type: "GET",
            success: function (data) {
                if(data== "success"){
                    loadDepo()
                    loadkuyu(id,serial,name)
                    swal({
                        icon: 'success',
                        title: 'Başarılı',
                        text: 'Depo Cihazı Eklendi',
                        button: "Tamam",
                    })
                }else{
                    swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: data,
                    button: "Tamam",
                })

                }
            },
            error: function (data) {
                swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: 'İşlem Başarısız Oldu',
                    button: "Tamam",
                })
            }
        });
        var islem = "NewDepoAdded";
        var cmd = randNumber + "/" + islem + "/" + serial;
        message = new Paho.MQTT.Message(cmd);
        message.destinationName = webToServerDestination;
        clientSend(message, randNumber, islem)
        clientSubscribe(serial);
        console.log(message);

        // var islem = "SettingsUpdated";
        // var cmd = randNumber + "/" + islem + "/" + serial;
        // message = new Paho.MQTT.Message(cmd);
        // message.destinationName = webToServerDestination; //DepoKuyu\Web
        // clientSend(message, randNumber, islem)
        // console.log(message);
        
    }
    //Yeni kuyu ekleme fonksiyonu
    function addKuyu() {
        var name = $('#txt_kuyu_name').val()
        var serial = $('#txt_kuyu_serial').val()
        var pompa_debi = $('#pompa_debi').val()
        var baslangic_saati = $('#txt_kuyu_bitis_saati').val()
        var bitis_saati = $('#txt_kuyu_baslangic_saati').val()
        var control_time = $("#txt_kuyu_control_time").val()
        var total_working_hours = $('#total_working_hours').val()
        var phone1 = $("#kuyu_phone_1").val()
        var phone2 = $("#kuyu_phone_2").val()
        var phone3 = $("#kuyu_phone_3").val()
        var email1 = $("#kuyu_email1").val()
        var email2 = $("#kuyu_email2").val()
        $.ajax({
            url: "./kuyu/store?name=" + name + "&serial=" + serial +"&pompa_debi=" +pompa_debi+ "&baslangic_saati=" +baslangic_saati+ "&bitis_saati=" +bitis_saati+ "&control_time=" +control_time+ "&total_working_hours="+total_working_hours+ "&phone1=" +phone1+ "&phone2" +phone2+ "&phone3" +phone3+ "&email1=" +email1+ "&email2=" +email2,
            type: "GET",
            success: function (data) {
                if(data== "success"){
                    loadDepo()
                    loadkuyu(id,serial,name);
                    swal({
                        icon: 'success',
                        title: 'Başarılı',
                        text: 'Kuyu Cihazı Eklendi',
                        button: "Tamam",
                    })
                }else{
                    swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: data,
                    button: "Tamam",
                })

                }
            },
            error: function (data) {
                swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: 'İşlem Başarısız Oldu',
                    button: "Tamam",
                })
            }
        });
        var islem = "NewKuyuAdded";
        var cmd = randNumber + "/" + islem + "/" + serial;
        message = new Paho.MQTT.Message(cmd);
        message.destinationName = webToServerDestination;
        clientSend(message, randNumber, islem)
        clientSubscribe(serial);
        console.log(message);

    } 
    //depo tablosunu dolduran foksiyon. Sayfa ilk açıldığında tam liste geliyor ancak herhangi bir güncelleme sonrası yenilemek için
    function loadDepo(id,serial,name){
        depoid= id;
        deposerial= serial;
        deponame= name
        $.ajax({
            url: './home/getalldepowithconnections',
            type: "GET",
            success: function(data){
                depotable.clear().draw();
                clearKuyuTable();
                $.each(data,function(index,item){
                    var activecircle = item.connected==true?"<i class='fa fa-circle' style='color:green'></i>":"<i class='fa fa-circle' style='color:red'></i>";
                    var row =$(
                        // "<tr><td><a class='btn btn-link m-0 p-0' style='width:100%' onclick='loadkuyu(" +item.id+","+item.serial+",\""+item.name+""'"
                             
                            "<tr><td><a class='btn btn-link m-0 p-0' style='width:100%' onclick='loadkuyu(" +
                            item.id + "," + item.serial + ",\"" + item.name + "\")'>"+activecircle+" "+ item
                            .name + "</a></td><td>" + item.serial + "</td><td>" + item
                            .depo_volume +"</td><td>" +item.childsize +"</td><td class='text-right'><div style='display:inline-flex'><button class='btn btn-link btn-warning btn-just-icon' onclick='goToDepoDetail(" + item.id + ",\"" + item.serial + "\" , \"" + item.name +"\","+item.depo_height+","+item.depo_volume+","+item.depo_max_level+","+item.depo_min_level+" ,"+item.baslangic_saati+" , "+item.bitis_saati+", "+item.control_time+")'><i class='fa fa-hourglass-half' aria-hidden='true'></i></button><button class='btn btn-link  btn-success btn-just-icon'onclick='opendepoeditmodal(" + item.id + ")'><i class='fa fa-pencil'></i></button></div></td></tr>"
                        );
                        depotable.row.add(row).draw();
                })
            },
            error: function(data) {
                if(data.status == 401){
                    refreshPage()
                };
            }
        })
        title = "";
        $("#kuyu-card-title").html(title);

    }
    //Seçilen depoya göre kuyuları getiren fonksiyon
    function loadkuyu(id,serial,name){
        depoid= id;
        deposerial= serial;
        deponame= name
        $.ajax({
            url : './home/getselectedkuyus/'+id,
            type: "GET",
            success: function(data){
                console.log(data)
                kuyutable.clear().draw();
                $.each(data,function(index,item){
                    
                    var activecircle = item.connected==true?"<i class='fa fa-circle' style='color:green'></i>":"<i class='fa fa-circle' style='color:red'></i>";
                    var row = $("<tr><td>"+activecircle+" "+item.name+"</td><td>"+item.serial+"</td><td>"+item.pompa_debi+"</td><td>"+item.childsize+"</td><td>"+item.total_working_hours+"</td>"+
                        '<td class="text-right"><div style="display:inline-flex"><button class="btn btn-link btn-warning btn-just-icon" onclick="KuyuKontrolModal(\''+item.serial+'\')">'+
                            "<i class='fa fa-cog' aria-hidden='true'></i></button>"+
                            "<button class='btn btn-link btn-success btn-just-icon' onclick = 'openkuyueditmodal("+item.id+")'>"+
                                "<i class='fa fa-pencil' aria-hidden='true'></i></button></div></td></tr>"
                    );
                    kuyutable.row.add(row).draw();
                })
            },
            error: function (data) {
                if (data.status == 401) {
                    refreshPage()
                };
            }

        })
        title = name + " --> Kuyular";
        $("#kuyu-card-title").html(title);

    }
    //Depo Düzenleme işlemi
    function opendepoeditmodal (id) {
        $('#edit-depo-modal').modal('show');

        $.ajax({
            url : './home/getdepo?id=' +id,
            type: "GET",
            success: function(data){
                console.log(data);
                $('#id').val(data.id);
                $('#fname').val(data.name);
                $('#serial').val(data.serial);
                $('#depo_volume').val(data.depo_volume);
                $('#depo_height').val(data.depo_height);
                $('#depo_max_level').val(data.depo_max_level);
                $('#depo_min_level').val(data.depo_min_level);
                $('#edit_baslangic_saati').val(data.baslangic_saati);
                $('#edit_bitis_saati').val(data.bitis_saati);
                $('#edit_depo_control_time').val(data.control_time);
                $('#edit_depo_phone1').val(data.phone1);
                $('#edit_depo_phone2').val(data.phone2);
                $('#edit_depo_phone3').val(data.phone3);
                $('#edit_depo_email1').val(data.email1);
                $('#edit_depo_email2').val(data.email2);

                //controllerda alnan değişkeni data. olarak çağırıldı.Tüm seçili kuyuları array oluşturdu.getirilen kuyular arrayin içine push edildi.daha sonra selec2 nin seçili getirme komutu uygulandı.
                var deposkuyus = data.deposkuyus;
                var selectedarrays = new Array();
                deposkuyus.forEach(item => {
                    selectedarrays.push(item.kuyu_id.toString());
                });
                $('#deposkuyusedit').val(selectedarrays);
                $('#deposkuyusedit').trigger('change');

                // $('#deposkuyusedit').select2('val',selectedarrays);
                 console.log(selectedarrays);
            },
        });
    }

    $('#editDepoForm').on('submit', function (e) {
        e.preventDefault();
        var id = $('#id').val();

        $.ajax({
            type: "PATCH",
            url: "./depo/update/" + id,
            data: $('#editDepoForm').serialize(),
            // dataType: "json"
            success: function (data) {
                // $('#depo_table').DataTable().ajax.reload();
                if(data== "success"){
                    var islem = "devicesSettingsUpdated";
                    var cmd = randNumber + "/" + islem + "/" + serial;
                    message = new Paho.MQTT.Message(cmd);
                    message.destinationName = webToServerDestination;
                    clientSend(message, randNumber, islem)
                    console.log(message);
                    loadDepo(id)
                    //location.reload(); // then reload the page.(3)
                    swal({
                        icon: 'success',
                        title: 'Başarılı',
                        text: 'Depo Cihazı Güncellendi',
                        button: "Tamam",
                    })
                }else{
                    swal({
                        icon: 'error',
                        title: 'Başarısız',
                        text: data,
                        button: "Tamam",
                    })
                }
                
            },
            error: function (data) {
                swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: 'İşlem Başarısız Oldu',
                    button: "Tamam",
                })
            }
        });
    });

    //Kuyu Düzenleme
    function openkuyueditmodal(id){
        console.log(id);
        $('#edit-kuyu-modal').modal('show');
            $.ajax({
                url : './home/getkuyu?id='+id,
                type: "GET",
                success: function(data){
                    console.log(data);
                    $('#id').val(data.id);
                    $('#kname').val(data.name);
                    $('#edit_kuyu_serial').val(data.serial);
                    $('#edit_pompa_debi').val(data.pompa_debi);
                    $('#kuyu_edit_baslangic_saati').val(data.baslangic_saati);
                    $('#kuyu_edit_bitis_saati').val(data.bitis_saati);
                    $('#kuyu_edit_control_time').val(data.control_time);
                    $('#edit_total_working_hours').val(data.total_working_hours);
                    $('#edit_kuyu_phone1').val(data.phone1);
                    $('#edit_kuyu_phone2').val(data.phone2);
                    $('#edit_kuyu_phone3').val(data.phone3);
                    $('#edit_kuyu_email1').val(data.email1);
                    $('#edit_kuyu_email2').val(data.email2);

                    var kuyusdepos = data.kuyusdepos;
                    var selectedkuyuarrays = new Array();
                    kuyusdepos.forEach(item => {
                        selectedkuyuarrays.push(item.depo_id.toString());
                    });
                    $('#kuyusdeposedit').val(selectedkuyuarrays);
                    $('#kuyusdeposedit').trigger('change');

                    // $('#deposkuyusedit').select2('val',selectedarrays);
                    console.log(selectedkuyuarrays);
                },

            })
    }

    $('#editKuyuForm').on('submit', function (e) {
        e.preventDefault();
        var id = $('#id').val();

        $.ajax({
            type: "POST",
            url: "./kuyu/update/" + id,
            data: $('#editKuyuForm').serialize(),
            success: function (response) {
                if(response.status == "success"){
                    loadkuyu(id,serial,name)
                    swal({
                        icon: 'success',
                        title: 'Başarılı',
                        text: 'Kuyu Cihazı Güncellendi',
                        button: "Tamam",
                    })
                    var islem = "devicesSettingsUpdated";
                    var cmd = randNumber + "/" + islem + "/" + response.serial;
                    message = new Paho.MQTT.Message(cmd);
                    message.destinationName = webToServerDestination;
                    clientSend(message, randNumber, islem)
                    console.log(message);
                }else{
                    swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: data,
                    button: "Tamam",
                })

                }
            },
            error: function (data) {
                swal({
                    icon: 'error',
                    title: 'Başarısız',
                    text: 'İşlem Başarısız Oldu',
                    button: "Tamam",
                })
            }
        });
    });

    function deletedepo(){
        var id = $('#id').val();
        console.log(id)
        swal({
            title: "Uyarı",
            text:"Gerçekten Silmek İstiyor Musunuz?",
            icon: "warning",
            buttons: {
                cancel: {
                    text:"Hayır",
                    visible: true,
                    className: "btn-success",
                    value: false
                },
                confirm: {
                    text: "Evet",
                    className: "btn-danger",
                    value:true
                }
            },
        })

        .then((willDelete)=>{
            if(willDelete){
                $.ajax({
                    url: "./depo/destroy/" +id,
                    type: "GET",
                    success: function(response){
                        console.log(response)
                        if(response.status=="success"){
                            loadDepo()
                            var islem = "DeleteDepo";
                            var cmd = randNumber + "/" + islem + "/" + response.serial;
                            message = new Paho.MQTT.Message(cmd);
                            message.destinationName = webToServerDestination;
                            clientSend(message, randNumber, islem)
                            console.log(message);
                            $("#edit-depo-modal").modal("hide");
                            swal({
                                icon: 'success',
                                title: 'Başarılı',
                                text: 'Depo Cihazı Silindi',
                                button:"Tamam"
                            })

                        }else{
                            swal({
                                icon:'error',
                                title:'Başarısız',
                                text:'Bu Cihazı Silmeye Yetkiniz Yoktur',
                                button: "Tamam"
                            })
                        }
                    },
                    error:function(data){
                        swal({
                            icon: 'error',
                            title: 'Başarısız',
                            text: 'İşlem Başarısız Oldu',
                            button: "Tamam"
                        })
                    }
                });
            }
        });
    }
    function deletekuyu(){
        var id = $('#id').val();
        console.log(id)
        swal({
            title: "Uyarı",
            text:"Gerçekten Silmek İstiyor Musunuz?",
            icon: "warning",
            buttons: {
                cancel: {
                    text:"Hayır",
                    visible: true,
                    className: "btn-success",
                    value: false
                },
                confirm: {
                    text: "Evet",
                    className: "btn-danger",
                    value:true
                }
            },
        })

        .then((willDelete)=>{
            if(willDelete){
                $.ajax({
                    url: "./kuyu/destroy/"+id,
                    type: "GET",
                    success: function(response){
                        console.log(response)
                        if(response.status=="success"){
                            loadDepo();
                            var islem = "DeleteKuyu";
                            var cmd = randNumber + "/" + islem + "/" + response.serial;
                            message = new Paho.MQTT.Message(cmd);
                            message.destinationName = webToServerDestination;
                            clientSend(message, randNumber, islem)
                            console.log(message);
                            $("#edit-kuyu-modal").modal("hide");
                            swal({
                                icon: 'success',
                                title: 'Başarılı',
                                text: 'Kuyu Cihazı Silindi',
                                button:"Tamam"
                            })

                        }else{
                            swal({
                                icon:'error',
                                title:'Başarısız',
                                text:'Bu Cihazı Silmeye Yetkiniz Yoktur',
                                button: "Tamam"
                            })
                        }
                    },
                    error:function(data){
                        swal({
                            icon: 'error',
                            title: 'Başarısız',
                            text: 'İşlem Başarısız Oldu',
                            button: "Tamam"
                        })
                    }
                });
            }
        });
    }
    //aşağıdaki fonksiyonların tamamı modalları açan fonksiyonlardır
    function clickAddKuyu() {
        $('#add-kuyu-modal').modal('show');
    }
    function clickAddDepo() {
        $('#add-depo-modal').modal('show');
    }
    //Depolar tablosundaki tüm verileri siler ve butonları disabled yapar
    function clearKuyuTable() {
        kuyutable.clear().draw();
        // $('btn-open-modal-add-kuyu').attr("disabled",true)
    }
    function clearDepoTable() {
        depotable.clear().draw();
        // $('btn-open-modal-add-kuyu').attr("disabled",true)
    }

</script>
@endsection