@extends('layouts.app',['page'=>'logs'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Gemiş Kayıtlar</h3>
        <hr style="border-color:#33333350">
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-12">
       <div class="card" style="border-radius: 30px">
              <div class="card-header card-header-primary card-header-icon">
                <h3 style="color:#333; margin-top:30px"><i class="fa fa-briefcase" style="color:green;vertical-align: initial; font-size: 30px;"></i></h3>
                <hr>
              </div>
            <div class="card-body">
              <div class="material-datatables">
                <table id="logs_table" class="table table-striped table-no-bordered table-hover text-center "  cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      
                      <th>Cihaz Serial</th>
                      <th>Cihaz İsmi</th>
                      <th>Yapılan İşlemler</th>
                      <th>İşlem Yapan Şirket</th>
                      <th>İşlem Yapan Kişi</th>
                      <th>Zaman</th>
                    </tr>
                  </thead>
                  <tbody>
                     {{-- @foreach ($logs as $item)
                      <tr>
                          <td>{{$item->device->serial}}</td>
                          <td>{{$item->device->name}}</td>
                          <td>{{$item->logrecord->islem}}</td>
                          <td>{{$item->company->name}}</td>
                          <td>{{$item->user->name}}</td> 
                          <td>{{$item->created_at}}</td> 
                      </tr>
                    @endforeach  --}}
                    @foreach ($logs as $item)
                      <tr>
                          <td>{{$item->device->serial}}</td>
                          <td>{{$item->device->name}}</td>
                          <td>{{$item->logrecord->islem}}</td>
                          {{-- <td>{{$item->logrecord->islem}}</td> --}}
                          <td>{{$item->company->name}}</td>
                          <td>{{$item->user->name}}</td> 
                          <td>{{$item->tarih}}</td> 
                      </tr>
                    @endforeach 
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</div>
    
@endsection
@section('scripts')
<script>
    var logstable = null;

    $(document).ready(function () {
      logstable=$('#logs_table').DataTable({
            reponsive: true,
            "pagingType": "numbers",
            "order": [1, "asc"],
            "lengthMenu": [
                [5, 10, -1],
                [5, 10, "All"]
            ],
            "language": {
                search: "Search",
                searchPlaceholder: "Search records",

            }
        });
    });
</script>
@endsection

