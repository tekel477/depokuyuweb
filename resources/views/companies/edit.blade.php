@extends('layouts.app',['page'=>'company'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Firma Hesapları</h3>
        <hr style="border-color:#33333350">
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
       <div class="card" style="border-radius: 30px">
        <div class="card-header card-header-primary card-header-icon">
            <h3 style="color:#333; margin-top:30px;"><i class="fa fa-pencil" style="color:green;vertical-align: initial; font-size: 30px;"></i>Firma Bilgilerini Düzenleme</h3>
               
            <hr>
        </div>
            <div class="card-body">
                 <form method="POST" action="{{route('companies.update',$companies->id)}}" id="myForm">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">Firma Adı</label>
                                <div class="col-md-9">
                                  <div class="form-group has-default">
                                  <input type="text" name="name" class="form-control" value="{{$companies->name}}">
                                  </div>
                                </div>
                        </div>
                        <div class="row">
                            <label class="col-md-2 col-form-label">Firma Unvanı</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                   <input type="text" name="unvan" class="form-control" value="{{$companies->unvan}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">Adres</label>
                                <div class="col-md-9">
                                  <div class="form-group has-default">
                                  <input type="text" name="address" class="form-control" value="{{$companies->address}}">
                                  </div>
                                </div>
                        </div>
                        <div class="row">
                            <label class="col-md-2 col-form-label">Telefon</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                   <input type="text" name="phone" class="form-control" value="{{$companies->phone}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    <div class="col-12 d-inline-block">
                        <button type="submit" class="btn btn-fill btn-success float-right" >Güncelle</button>
                    </div>
                </div>
                </form>
                
            </div>
       </div>
    </div>
</div>
@endsection