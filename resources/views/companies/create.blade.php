@extends('layouts.app',['page'=>'company'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Firma Hesapları</h3>
        <hr style="border-color:#33333350">
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
        <div class="card" style="border-radius: 30px">
            <div class="card-header card-header-primary card-header-icon">
                <h3 style="color:#333; margin-top:30px;"><i class="fa fa-plus-square" style="color:green;vertical-align: initial; font-size: 30px;"></i> Yeni Firma Ekleme</h3>
                <hr>
            </div>
            <div class="card-body">
            <form method="POST" action="{{route('companies.store')}}" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 col-form-label">Firma Adı</label>
                                <div class="col-md-9">
                                    <div class="form-group ">
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Firma Unvanı</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="unvan" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 col-form-label">Adres</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="address" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Telefon</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="text" name="phone" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="card-footer">
                    @csrf
                    <div class="col-12 d-inline-block">
                        <button type="submit" class="btn btn-fill btn-success float-right">Kaydet</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')


@endsection