@extends('layouts.app',['page' => 'company'])
@section('content')
    <div class="row">
        <div class="col-12">
            <h3 class="title-container text-dark">Firma Hesapları</h3>
            <hr style="border-color:#33333350">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
           <div class="card" style="border-radius: 30px">
                  <div class="card-header card-header-primary card-header-icon">
                    <h3 style="color:#333; margin-top:30px"><i class="fa fa-address-card" style="color:green;vertical-align: initial; font-size: 30px;"></i> Firma Listesi</h3>
                    <a class="btn btn-success btn-sm float-right" href="{{route('companies.create')}}">Yeni Oluştur</a>
                    <hr>
                  </div>
                <div class="card-body">
                  <div class="material-datatables">
                    <table id="company_table" class="table table-striped table-no-bordered table-hover text-center "  cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th>Firma Adı</th>
                          <th>Firma Unvanı</th>
                          <th>Adres</th>
                          <th>Telefon</th>
                          <th class="disabled-sorting text-right">İşlemler</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($companies as $item)
                          <tr>
                             <input type="hidden" class="btndelete_val" value="{{$item->id}}">
                              <td>{{$item->name}}</td>
                              <td>{{$item->unvan}}</td>
                              <td>{{$item->address}}</td>
                              <td>{{$item->phone}}</td>
                              <td class='text-right'>
                              <a href="{{route('companies.edit',$item->id)}}" class="btn btn-link btn-info btn-just-icon edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove deletebtn"><i class="fa fa-close" aria-hidden="true"></i></a>
                              <form id="company-destroy-{{$item->id}}" action="{{ route('companies.destroy',$item->id) }}" method="POST" style="display: none;">
                                 @csrf
                              </form>
                              </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
var companytable=null;

$(document).ready(function(){
    usertable = $('#company_table').DataTable({
      reponsive:true,
      "pagingType":"numbers",
      "order":[1,"asc"],
      "lengthMenu": [
        [10,25,-1],
        [10,25,"All"]
      ],
      language:{
        search: "Search",
        searchPlaceholder: "Search records",
      }
    });
});
$('#company_table tbody').on('click','.deletebtn',function(e){
      e.preventDefault();
      var delete_id = $(this).closest("tr").find('.btndelete_val').val();
      // alert(delete_id)
      swal({
        title: "Emin misiniz?",
        text: "Firmayı Gerçekten Silmek İstiyor Musunuz?",
        icon: "error",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $("#company-destroy-"+delete_id).submit();
        } else {
          swal("İşlem İptal Edildi!");
        }
      });
    });

</script>
@endsection
