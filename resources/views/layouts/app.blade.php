
<!DOCTYPE html>
<html lang="en">

<head>
  <title>DepoKuyu Sistemi</title> 
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" type="img/png" href="img/argerf.png">
  
  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link href="{{asset('assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
  <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css"/> 
  <link href="{{asset('assets/css/nucleo-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/css/loading.css')}}" rel="stylesheet">
  <link href="{{asset('assets/css/loading-progress.css')}}" rel="stylesheet">
  <link href="{{asset('assets/css/select2/select2.min.css')}}" rel="stylesheet">
 
</head>

<body>
  
  <div class="wrapper ">
    <div class="sidebar" data-color="azure" data-background-color="black" data-image="{{asset('assets/img/sidebar-1.jpg')}}">
      <div class="logo">
        <a href="javascript:void(0)" class="simple-text logo-mini">
          DK
        </a>
        <a href="javascript:void(0)" class="simple-text logo-normal">
          DEPO KUYU
        </a>
      </div>
      <div class="sidebar-wrapper">
          <ul class="nav">
              @if ($page =="home")
              <li class="nav-item active">
                @else
              <li>
                @endif
                <a class="nav-link" href="{{route('home')}}">
                  <i class="fa fa-rss" aria-hidden="true"></i>
                <p>Kontrol Merkezi</p>
                </a>
                <hr style=" margin:0px 60px; border-top: 1px solid rgba(255, 255, 255, 0.2);">
              </li>
              @if ($page =="acount")
              <li class="nav-item active">
                @else
              <li>
                @endif
                <a class="nav-link" href="{{route('users.index')}}">
                  <i class="fa fa-users" aria-hidden="true"></i>
                <p>Kullanıcı Hesapları</p>
                </a>
                <hr style=" margin:0px 60px; border-top: 1px solid rgba(255, 255, 255, 0.2);">
              </li>
              @if(Auth::user()->role_id=='1')
                @if ($page =="company")
                <li class="nav-item active">
                  @else
                <li>
                  @endif
                <a class="nav-link" href="{{route('companies.index')}}">
                  <i class="fa fa-building-o" aria-hidden="true"></i>
                <p>Firma Bilgileri</p>
                </a>
                <hr style=" margin:0px 60px; border-top: 1px solid rgba(255, 255, 255, 0.2);">
              </li>
              @endif 
              @if ($page =="acountsettings")
              <li class="nav-item active">
                @else
              <li>
                @endif
                <a class="nav-link" href="{{route('users.currentacount')}}">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
                <p>Hesap Ayarları</p>
                </a>
                <hr style=" margin:0px 60px; border-top: 1px solid rgba(255, 255, 255, 0.2);">
              </li>
              @if(Auth::user()->role_id=='1')
                @if($page=='settings')
                <li class="nav-item active">
                  @else
                <li>
                  @endif
                <a class="nav-link" href="{{route('settings')}}">
                  <i class="fa fa-gears" aria-hidden="true"></i>
                  <p>Sistem Ayarları</p>
                </a>
                <hr style=" margin:0px 60px; border-top: 1px solid rgba(255, 255, 255, 0.2);">
                </li>
                @endif
              @if($page=='logs')
              <li class="nav-item active">
                @else
              <li>
                @endif
              <a class="nav-link" href="{{route('logs.index')}}">
                <i class="fa fa-files-o" aria-hidden="true"></i>
                <p>Geçmiş Kayıtlar</p>
              </a>
              <hr style=" margin:0px 60px; border-top: 1px solid rgba(255, 255, 255, 0.2);">
              </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
              <p>Çıkış Yap</p>
              </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @method('POST')
                    @csrf
                </form>
            </li>
          </ul>
      </div>
    </div>
      <div class="main-panel" data="blue">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent ">
              <div class="container-fluid">
                  <div class="navbar-wrapper d-inline">
                    <div class="navbar-minimize">
                      <button id="minimizeSidebar" class="btn btn-just-icon btn-dark btn-fab btn-round">
                        <i class="fa fa-tasks"></i>
                        <i class="fa fa-tasks"></i>
                      </button>
                    </div>
                  </div> 
                  <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                  </button>
              </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
          @yield('content')
        </div>
        {{-- <script src="text/javascript" src="{{asset('js/app.js')}}"></script>   --}}
        <footer class="footer">
          <div style="margin-top:-1rem">
            <div class="container-fluid">
              <ul class="nav">
                <li class="nav-item">
                  <a href="javascript:void(0)" class="nav-link">
                    DepoKuyu
                  </a>
                </li>
              </ul>
              <div class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script> made with By
                <b>
                    <a href="https://www.argebilisim.com.tr/" target="_blank">ArgeRF</a>
                </b>
              </div>
            </div>
          </div>
        </footer>
      </div>
  </div> 
{{-- </div> 
</div>  --}}

              
              <script src="{{asset('assets/js/core/jquery.min.js')}}"></script> 
              <script src="{{asset('assets/js/core/popper.min.js')}}"></script> 
              <script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}"></script>
              <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
              <!-- Plugin for the momentJs  -->
              <script src="{{asset('assets/js/plugins/moment.min.js')}}"></script>
              <!--  Plugin for Sweet Alert -->
              <script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
              <!-- Forms Validations Plugin -->
              <script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
              <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
              <script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
              <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
               <script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script> 
              <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
              <script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
              <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
               <script src="{{asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
              <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
              <script src="{{asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
              <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
              <script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
              <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
              <script src="{{asset('assets/js/plugins/fullcalendar.min.js')}}"></script>
              <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
              <script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
              <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
              <script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
              <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
              <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
              <!-- Library for adding dinamically elements -->
              <script src="{{asset('assets/js/plugins/arrive.min.js')}}"></script>
              <!-- Chartist JS -->
              <script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
              <!--  Notifications Plugin    -->
              <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
              <script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
              <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
              <script src="{{asset('assets/js/material-dashboard.js?v=2.1.0')}}" type="text/javascript"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script> 
              <script src="{{asset('assets/js/Bootstrap/js/bootstrap.min.js')}}" ></script> 
              <script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
              <script src="{{asset('assets/js/mqttws31.min.js')}}"></script>
              <script src="{{asset('assets/js/mqtt-settings.js')}}"></script>
              <script>
                function goBack() {
                  window.history.back();
                }
                function refreshPage(){
                    location.reload();
                }
              </script>
            
              @yield('scripts')

              <script>
                @if (session('status'))
                swal({
                  title: '{{ session('status')}}',
                  // text: "Kullanıcı Eklendi",
                  icon: '{{session('statuscode')}}',
                  button: "Tamam",
                });
                @endif 
              </script> 
                {{-- <script>
                $(document).ready(function(){
                  $('.logout').click(function(e){
                    e.preventDefault();
              
                    $("#logout-form").submit();
                  });
              
                })
              </script> --}}
              
               {{-- <script>
                client = new Paho.MQTT.Client('83.150.214.186', 8083, "Depo-Kuyu-Test" + Math.random()*1000);
                client.connect({
                    onSuccess:onConnect
                });
    
                function onConnect(){
                    console.log("onConnect");
    
                    client.subscribe("WORLD");
                    message = new Paho.MQTT.Message("deneme");
                    message.destinationName = "WORLD";
                    setInterval(function(){
                        client.send(message);
                    },1000)
                    

                }

                
            </script>  --}}
            

             

            
              

              
              
             
</body>

</html>
