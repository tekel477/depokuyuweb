@extends('layouts.app',['page'=>'acount'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Kullanıcı Hesapları</h3>
        <hr style="border-color:#33333350">
         {{-- @if (isset($response))
             {{dd($response)}} 
            @if ($response["status"]=="success")
                <div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    <span><b> Tebrikler - </b> {{$response["message"]}}</span>
                </div>
            @else
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    <span><b> Başarısız - </b> {{$response["message"]}}</span>
                </div>
            @endif
        @endif  --}}

         {{-- @if (isset($response) &&$response["status"]=="error")
            <div class="alert alert-danger">
                <span><b> Başarısız - </b> {{$response["message"]}}</span>
            </div>
        @endif  --}}
        {{-- @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Başarılı</strong>{{session()->get('message')}}
        </div>
        @endif --}}
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
        <div class="card" style="border-radius: 30px">
            <div class="card-header card-header-primary card-header-icon">
                <h3 style="color:#333; margin-top:30px;"><i class="fa fa-plus-square" style="color:green;vertical-align: initial; font-size: 30px;"></i> Yeni Kullanıcı Ekleme</h3>
                <hr>
            </div>
            <div class="card-body">
            <form method="POST" action="{{route('users.store')}}" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 col-form-label">İsim Soyisim</label>
                                <div class="col-md-9">
                                    <div class="form-group ">
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Şifre</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Telefon</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="phone" name="phone" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 col-form-label">Email</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Kullanıcı Statüsü</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                    <select name="role_id" id="role_id" class="selectpicker form-control" required>
                                        @foreach ($roles as $role)
                                        <option value="{{$role->id}}">{{$role->role_status}}</option>
                                        @endforeach
                                    </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 col-form-label">Bağlı Firma</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                    <select name="company_id" id="company_id" class="selectpicker form-control" required >
                                        {{-- @if(Auth::user()->company_id == Auth::user()->id) --}}
                                        @if(Auth::user()->role_id=='1')
                                            @foreach ($companies as $company)
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        @endif
                                    </select> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="card-footer">
                    @csrf
                    <div class="col-12 d-inline-block">
                        <button type="submit" class="btn btn-fill btn-success float-right">Kaydet</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection

