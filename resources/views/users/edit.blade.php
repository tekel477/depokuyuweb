@extends('layouts.app',['page'=>'acount'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Kullanıcı Hesapları</h3>
        <hr style="border-color:#33333350">
        {{-- @if(session()->has('message'))
        <div class="alert alert-danger" role="alert">
            <strong></strong>{{session()->get('message')}}
        </div>
        @endif --}}
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-12">
       <div class="card" style="border-radius: 30px">
        <div class="card-header card-header-primary card-header-icon">
            <h3 style="color:#333; margin-top:30px;"><i class="fa fa-pencil" style="color:green;vertical-align: initial; font-size: 30px;"></i> Kullanıcı Düzenleme</h3>
               {{-- <a class="btn btn-danger btn-sm float-right" href="{{ route('user.destroy',$users->id) }}" onclick="event.preventDefault(); document.getElementById('user-destroy').submit();">Kullanıcıyı Sil</a>
               <form id="user-destroy" action="{{ route('user.destroy',$users->id) }}" method="POST" style="display: none;">
                   @csrf
               </form> --}}
               
               {{-- <a class="btn btn-danger btn-sm float-right deletebtn" href="{{route('user.destroy',$users->id)}}">Kullanıcıyı Sil</a> //Form işlemi yapmadan edit sayfasında silme işlemi.Ajax ile yine bu sayfadan gönderilebilirdi --}}
            <hr>
            {{-- @if (isset($response))
            @if ($response["status"]=="success")
                <div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    <span><b> Tebrikler - </b> {{$response["message"]}}</span>
                </div>
            @else
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    <span><b> Başarısız - </b> {{$response["message"]}}</span>
                </div>
            @endif
        @endif --}}

        </div>
            <div class="card-body">
                 <form method="POST" action="{{route('users.update',$users->id)}}" id="myForm">
                    {{ csrf_field() }}
                    {{method_field('PATCH')}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">İsim Soyisim</label>
                                <div class="col-md-9">
                                  <div class="form-group has-default">
                                  <input type="text" name="name" class="form-control" value="{{$users->name}}">
                                  </div>
                                </div>
                        </div>
                        <div class="row">
                            <label class="col-md-2 col-form-label">Email</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                   <input type="email" name="email" class="form-control" value="{{$users->email}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-form-label">Kullanıcı Statüsü</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <select class="selectpicker form-control" name="role_id" id="role_id">
                                        @foreach ($roles as $role)
                                            <option value="{{$role->id}}" {{$role->id == $users->role_id ? 'selected' : '' }}>{{$role->role_status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-2 col-form-label">Telefon</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                   <input type="phone" name="phone" class="form-control" value="{{$users->phone}}">
                                </div>
                            </div>
                        </div>
                    </div>
                        
                </div>
                <div class="card-footer">
                    @csrf
                    <div class="col-12 d-inline-block">
                        <button type="submit" class="btn btn-fill btn-success float-right" >Güncelle</button>
                    </div>
                </div>
                </form>
                {{--Kullanıcıyı düzenlerken yetkili kişi Yeni şifre verebilecek--}}
                <hr style="border-color:#33333350">
                <h4 style="color:#333; margin:0px">Şifre Sıfırlama</h4>
                <hr>
            <form id="reset_Password" method="POST" action="{{route('users.resetPassword',$users->id)}}">
                {{ csrf_field() }}
                    {{method_field('PATCH')}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">Yeni Şifre</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">Yeni Şifre Tekrarı</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                   <input type="password" name="password_again" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    @csrf
                    <div class="col-12 d-inline-block">
                        <button type="submit" class="btn btn-fill btn-success float-right" >Güncelle</button>
                    </div>
                </div>
            </form>
            </div>
       </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection 