@extends('layouts.app',['page'=>'acountsettings'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Kişisel Hesap Ayarları</h3>
        <hr style="border-color: #33333350">
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card" style="border-radius:30px">
            <div class="card-header card-header-primary card-header-icon">
                <h3 style="color:#333; margin-top:30px"><i class="fa fa-pencil" style="color:green;vertical-align: initial; font-size: 30px;"></i>Mevcut Hesap Düzenle</h3>
                <hr>
            </div>
            <div class="card-body">
                <form method="post" action="{{route('users.acountupdate', $users->id)}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 col-form-label">İsim Soyisim</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                    <input type="text" name="name" class="form-control" value="{{$users->name}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 col-form-label">Email</label>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" value="{{$users->email}}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        {{ csrf_field() }}
                        <div class="col-12 d-inline-block">
                            <button type="submit" class="btn btn-fill btn-success float-right">Kaydet</button>
                        </div>
                    </div>
                </form>
                <hr style="border-color:#33333350">
                <h4 style="color:#333; margin-top:30px">Şifre Değiştirme</h4>
                <hr>
            <form id="password-reset" method="post" action="{{route('users.changepassword',$users->id)}}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">Eski Şifre</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                  <input type="password" name="password_old" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-2 col-form-label">Yeni Şifre Tekrar</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="password" name="password_again" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-2 col-form-label">Yeni Şifre</label>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    @method('PATCH')
                    {{csrf_field()}}
                    <div class="col-12 d-inline-block">
                        <button type="submit" class="btn btn-fill btn-success float-right">Kaydet</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@endsection