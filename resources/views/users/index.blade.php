@extends('layouts.app',['page'=>'acount'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Kullanıcı Hesapları</h3>
        <hr style="border-color:#33333350">
        {{-- @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong></strong>{{session()->get('message')}}
        </div>
        @endif --}}
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-12">
       <div class="card" style="border-radius: 30px">
              <div class="card-header card-header-primary card-header-icon">
                <h3 style="color:#333; margin-top:30px"><i class="fa fa-address-card" style="color:green;vertical-align: initial; font-size: 30px;"></i> Kullanıcı Listesi</h3>
                <a class="btn btn-success btn-sm float-right" href="{{route('users.create')}}">Yeni Oluştur</a>
                <hr>
              </div>
            <div class="card-body">
              <div class="material-datatables">
                <table id="user_table" class="table table-striped table-no-bordered table-hover text-center "  cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th>İsim</th>
                      <th>Email</th>
                      <th>Role Seviyesi</th>
                      <th>Bağlı Firma</th>
                      <th>Telefon</th>
                      <th class="disabled-sorting text-right">İşlemler</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $item)
                      <tr>
                         <input type="hidden" class="btndelete_val" value="{{$item->id}}">
                          <td>{{$item->name}}</td>
                          <td>{{$item->email}}</td>
                          <td>{{$item->role_id}}</td>
                          <td>{{$item->company->name}}</td>
                          <td>{{$item->phone}}</td>
                          <td class='text-right'>
                          <a href="{{route('users.edit',$item->id)}}" class="btn btn-link btn-info btn-just-icon edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                          <a href="#" class="btn btn-link btn-danger btn-just-icon remove deletebtn"><i class="fa fa-close" aria-hidden="true"></i></a>
                          <form id="user-destroy-{{$item->id}}" action="{{ route('user.destroy',$item->id) }}" method="POST" style="display: none;">
                             @csrf 
                          </form>
                          </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
  var usertable=null;

  $(document).ready(function(){
    usertable = $('#user_table').DataTable({
      reponsive:true,
      "pagingType":"numbers",
      "order":[1,"asc"],
      "lengthMenu": [
        [10,25,-1],
        [10,25,"All"]
      ],
      language:{
        search: "Search",
        searchPlaceholder: "Search records",
      }
    });

    $('#user_table tbody').on('click','.deletebtn',function(e){
      e.preventDefault();
      var delete_id = $(this).closest("tr").find('.btndelete_val').val();
      // alert(delete_id)
      swal({
        title: "Emin misiniz?",
        text: "Kullanıcıyı Gerçekten Silmek İstiyor Musunuz?",
        icon: "error",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $("#user-destroy-"+delete_id).submit();
        } else {
          swal("İşlem İptal Edildi!");
        }
      });
    });
    
    // $('.deletebtn').click(function (e){
    //   e.preventDefault();
    //   var delete_id = $(this).closest("tr").find('.btndelete_val').val();
    //   // alert(delete_id)
    //   swal({
    //     title: "Emin misiniz?",
    //     text: "Kullanıcıyı Gerçekten Silmek İstiyor Musunuz?",
    //     icon: "error",
    //     buttons: true,
    //     dangerMode: true,
    //   })
    //   .then((willDelete) => {
    //     if (willDelete) {
    //       $("#user-destroy-"+delete_id).submit();
    //     } else {
    //       swal("İşlem İptal Edildi!");
    //     }
    //   });
    // });

  });
</script>

@endsection
