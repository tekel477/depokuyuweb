@extends('layouts.login',[
    'page_title' => 'Login'
])
@section('form')
<form class="form" method="POST" action="{{route('login')}}">
    @csrf
            <div class="card-header card-header-info text-center" >
                <h4 class="card-title text-center">DepoKuyu</h4>
                <h5 class="card-title text-center">Giriş Yap</h5>
            </div>
                    
                    <div class="card-body">
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">mail</i>
                                </span>
                            </div>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="E-posta" requried value="{{ old('email')}}" autofocus>
                            @error('email')
                            <span class="invalid-feedback ml-5" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                
                        <div class="input-group mt-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                            </div>
                            <input type="password" class="form-control" placeholder="Parola" name="password" required>
                            @error('password')
                            <span class="invalid-feedback ml-5" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <button class="btn btn-info">GİRİŞ</button>
                            </div>

                            <div class="col-12 text-center mt-5">
                                <a href="https://argebilisim.com.tr/" style="color:black">ARGERF ELEKTRONİK OTOMASYON YAZILIM SAVUNMA SAN. VE TİC.LTD. ŞTİ.</a>
                                {{-- <a href="" class="text-info" style="font-size: 0.7rem"><i class="material-icons">info</i> Forgot Password </a> --}}
                            </div>
                
                

                                    {{-- <div class="form-group row">
                                        <div class="col-md-6 offset-md-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Login') }}
                                            </button>

                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div> --}}
                        </div>
                    </div>
</form>
@endsection
@section('script')
@endsection
