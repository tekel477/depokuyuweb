@extends('layouts.app',['page'=>'settings'])
@section('content')
<div class="row">
    <div class="col-12">
        <h3 class="title-container text-dark">Sistem Çalışma Ayarları</h3>
        <hr style="border-color:#33333350">
        @if(isset($response))
            @if($response["status"]=="success")
                <div class="alert alert-success">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="fa fa-remove"></i></button>
                        <span><b> Tebrikler -</b> {{$response["message"]}}</span>
                </div>
            @else
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="close">
                        <i class="fa fa-remove"></i>
                    </button>
                    <span><b>Başarısız -</b> {{$response["message"]}}</span>
                </div>
            @endif
        @endif            
    </div>
</div>
<div class="row justify-content-center">
        <div class="col-12">
            <div class="card bg-white" style="border-radius: 30px">
                <div class="card-header">
                    <h3 style="color:#333; margin:0px"><i class="fa fa-cog" style="color:green;vertical-align: initial; font-size: 30px;"></i> Ayarlar</h3>
                    <hr>
                </div>
            <div class="card-body">
                <form id="settings_form" method="GET" action="{{route('settings.update',$settings->id)}}" autocomplete="off">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-2 col-form-label">MQTT Ip: </label>
                                    <div class="col-md-9">
                                        <div class="form-group ">
                                        <input type="text" name="mqtt_ip" class="form-control" placeholder="Exp: 192.168.1.2" value="{{$settings->mqtt_ip}}" required >
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label">MQTT Port:</label>
                                    <div class="col-md-9">
                                        <div class="form-group ">
                                        <input type="text" name="mqtt_port" class="form-control" placeholder="Exp: 8083" value="{{$settings->mqtt_port}}" required >
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            {{-- cihazların toplu gönderimi için sms ve mail giriniz --}}
                                <div class="col-md-6"> 
                                    <div class="row">
                                        <label class="col-md-2 col-form-label">Toplu SMS Numarası:</label>
                                        <div class="col-md-9">
                                            <div class="form-group ">
                                            <input type="text" name="top_sms_phone" class="form-control" placeholder="Toplu SMS için numaranızı Giriniz.." value="{{$settings->top_sms_home}}" required>
                                            </div>
                                        </div>
                                        <label class="col-md-2 col-form-label">Toplu MAİL Numarası:</label>
                                        <div class="col-md-9">
                                            <div class="form-group ">
                                            <input type="text" name="top_mail_email" class="form-control" placeholder="Toplu mail için Email Giriniz.." value="{{$settings->top_mail_email}}" required>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                        </div>
                    <div class="card-footer">
                        {{csrf_field()}}
                        <div class="col-12 d-inline-block">
                            <button id="btn_save" type="button" class="btn btn-fill btn-success float-right">Kaydet</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error-connection-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="margin-top: 100px">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content bg-light">
            <div class="modal-body text-center">
                <h3 class="text-dark mb-2">İnternet bağlantınızı kontrol edin!</h3>
                <button type="button" class="btn btn-secondary" onclick="refreshPage()">Tekrar Dene</button><br>
                <button type="button" class="btn btn-sm btn-danger" onclick="errormodalOFF()">Devam Et</button><br>
                <span>Lütfen internet bağlantınızı ve cihaz ayarlarını kontrol edin.</span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="loading-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: rgba(0, 29, 57,0.95);border-radius:40px;">
            <div class="modal-body text-center" style="height:30em; padding-left:6em; padding-top:9em">
                <div id="loading bg-transparent" style="">
                    <div id="preloader">
                        <div id="loader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('scripts')
<script>
    $(document).ready(function(){
        var mqttValues = {
            ip : "{{$settings->mqtt_ip}}",
            port: "{{$settings->mqtt_port}}",
            user_name : "DepoKuyuWeb",
            password : "depokuyuweb"

        }
        // mqttConnect(mqttValues.ip,Number(mqttValues.port),"settings"); //mqtt te bağlamaya gerek yok
    });

    $("#btn_save").click(function(){
        var post_url = "{{route('settings.update',$settings->id)}}"
        var request_method = "GET"
        var form_data = $("#settings_form").serialize(); 
        $.ajax({
            url : post_url,
            type : request_method,
            data : form_data,
            success:function(data){
                if(data== "success"){
                    swal({
                        icon: 'success',
                        title: 'Başarılı',
                        text: 'MQTT Bilgileri Güncellendi',
                        butoton: "Tamam",
                    })
                }else{
                    swal({
                        icon: 'error',
                        title: 'Başarısız',
                        text: data,
                        button: "Tamam",
                    })
                }
                var islem = "SettingsUpdated"
                var cmd = randNumber + "/" + islem
                message = new Paho.MQTT.Message(cmd);
                message.destinationName = webToServerDestination;
                clientSend(message,randNumber,islem)
            },
            error : function(data){
                // 
                if(data.status == 401){
                    refreshPage()
                }else{
                    $.notify({
                        message: "<b>Hata!!</b> - Beklenmeyen Bir Hata Oluştu.Lütfen Tekrar Deneyiniz"
                    },{
                        type: "danger",
                        timer: 5000
                    })
                }
            }
        });
    });
    // function stopProcessWait(){

    // }
    // function stopStatusWait(){
            
    //     }

</script>
@endsection