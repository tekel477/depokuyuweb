<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class Setting extends Migration
{

    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('baslik');
            $table->string('aciklama');
            $table->string('keyword');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('instagram');
            $table->string('youtube');
            $table->string('ayarlogo');
            $table->string('email');
            $table->string('adres');
            $table->timestamps();
           
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
