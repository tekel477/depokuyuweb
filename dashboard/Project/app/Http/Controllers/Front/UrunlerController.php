<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facedes\DB;
use Illuminate\Support\Facedes\Auth; 


use App\Models\Product;

class UrunlerController extends Controller
{
    
    public function index()
    {
        $post=Product::all();
        return view('products.index',compact('post'));//wep.php de urunler/index falan yapıp sayfanın gelip gelmediğine bak
    }

    
    public function create()
    {
        return view('products.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'urun_kod'=>'required',
            'ad' => 'required'
        ]);
        Product::create([
            'urun_kod' => $request->urun_kod,
            'ad' => $request->ad

        ]);

        return redirect()->route('products.index')->with('info','Products Added Successfuly');
    }

    
    public function show($id)
    {
        $post=Product::findOrfail($id);
        return view('products.details',compact('post'));

    }

    
    public function edit($id)
    {
        $post=Product::find($id);
        return view('products.edit',compact('post'));
    }

    
    public function update(Request $request, $id)
    {
        $post=Product::find($id);
        $post->ad = $request->get('ad');
        $post->urun_kod = $request->get('urun_kod');
        //dd($post);
        $post->save();
        return redirect()->route('products.index')->with('info','Products Updated Succesfully');
    }

    
    public function destory($id)
    {
        $post=Product::find($id);
        $post->delete();
        //toastr()->success('Makale Başarıyla Silindi');
        return redirect()->route('products.index');
    }
}
