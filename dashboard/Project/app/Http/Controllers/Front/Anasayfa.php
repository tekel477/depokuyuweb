<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Anasayfa extends Controller
{
    public function anasayfa(){
        return view('front.layouts.anasayfa');
    }
}

