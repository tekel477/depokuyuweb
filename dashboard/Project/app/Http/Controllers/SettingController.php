<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facedes\DB;
use Illuminate\Support\Facedes\Auth; 

use App\Models\Setting;


class SettingController extends Controller
{
    
    public function index()
    {
        $post=Setting::all();
       return view('settings.index',compact('post'));
       
       
    }

    
    public function create()
    {
       return view('settings.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'baslik'=>'required',
            'aciklama'=>'required',
            'keyword'=>'required',
            'instagram'=>'required',
            'facebook'=>'required',
            'youtube'=>'required',
            'twitter'=>'required',
            'ayarlogo'=>'required',
            'email'=> 'required',
            'adres'=> 'required'
        ]);
        Setting::create([
        'baslik'=> $request->baslik,
        'aciklama'=> $request->aciklama,
        'keyword' => $request->keyword,
        'instagram'=> $request->instagram,
        'facebook'=> $request->facebook,
        'youtube'=> $request->youtube,
        'twitter'=> $request->twitter,
        'ayarlogo'=> $request->ayarlogo,
        'email'=> $request->email,
        'adres'=> $request->adres
        
        
      ]);

      //$post->save();
      return redirect()->route('settings.index')->with('info','Settings Added Succesfully');

    }

    
    public function show($id)
    {
        
    }

    
    public function edit($id)
    {
        $post=Setting::find($id);
        return view('settings.edit',compact('post'));
    }

    
    public function update(Request $request, $id)
    {
          $post=Setting::find($id);
          $post->baslik = $request->get('baslik');
          $post->aciklama = $request->get('aciklama');
          $post->keyword= $request->get('keyword');
          $post->instagram = $request->get('instagram');
          $post->youtube = $request->get('youtube');
          $post->facebook = $request->get('facebook');
          $post->twitter = $request->get('twitter');
          $post->ayarlogo= $request->get('ayarlogo');
          $post->email = $request->get('email');
          $post->adres = $request->get('adres');
          $post->save();
          //dd($post); 
          return redirect()->route('settings.index')->with('info','Settings Updated Successfully');
          
    }

    
    public function destory($id)
    {
        $post=Setting::find($id);
        $post->delete();
        return redirect()->route('settings.index');
    }
}
