<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='settings';

    protected $guarted=['id'];

    protected $fillable=['baslik','ayarlogo','email','adres','aciklama','keyword','instagram','youtube','facebook','twitter'];

           
    
}
