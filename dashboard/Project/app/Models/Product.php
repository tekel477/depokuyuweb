<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';

    protected $guarted=['id'];

    protected $fillable=['urun_kod','ad'];
}
