<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogRecord extends Model
{
    protected $table = 'log_records';
    protected $guarded = ['id'];

    public function log(){
        return $this->hasMany('App\Log','id');
    }
}
