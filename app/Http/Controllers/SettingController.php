<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Setting;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
            if(Auth::user()->role_id=='1' || Auth::user()->role_id == 2){
                $settings = Setting::get()->first();
                return view('settings.index',compact("settings"));
            }else{
                return redirect()->route('home');
            }
    }

    
    public function update(Request $request, $id){
        if(Auth::user()->role_id =="1"){
            $settings= Setting::get()->first();
            $settings->mqtt_ip = $request->mqtt_ip;
            $settings->mqtt_port = $request->mqtt_port;
            $settings->mqtt_user_name = $request->mqtt_user_name;
            $settings->mqtt_user_password = $request->mqtt_user_password;
            $settings->top_sms_phone = $request->top_sms_phone;
            $settings->top_mail_email = $request->top_mail_email;
            $settings->save();Session::flash('statuscode','info');
            return 'success';
        }else{
            Session::flash('statuscode','error');
            return redirect('/settings')->with('status','Güncellenemedi');
        }
    }
   
}
