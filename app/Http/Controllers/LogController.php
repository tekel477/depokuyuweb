<?php

namespace App\Http\Controllers;


use App\Log;
use App\Logg;
use App\LogRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id=='1'){
            $logs =Log::all();
            return view('logs.index',compact('logs'));
        }else{

            $logs =  Log::Where('user_id',Auth::user()->user_id)->get();
            
            return view('logs.index',compact('logs'));
        }
        //  $logs=Log::Where("user_id",Auth::user()->user_id)->orderBy('created_at', 'desc')->get(); //company_id yapılabilir
        $logs = Log::get();
        return view('logs.index',compact('logs'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function Logg()
    // {
    //     try{
    //         if($_GET["command"]!="" && $_GET["deviceid"]!="" && $_GET["companyid"]!="" && $_GET["userid"]!="")
    //         {
    //             $command=$_GET["command"]==0?"Kuyu Kontrol => açık":"Door Control => kapalı";
    //             return ($this->AddLog($_GET["deviceid"],$_GET["outspoint"],$command));
    //         }
    //     }catch(Exception $ex){
    //         return $ex;
    //     }
    // }


}
