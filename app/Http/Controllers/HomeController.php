<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Response;
use App\Device;
use App\DepoKuyu;
use App\Setting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $depos = $this->getAllDepoWithConnections();
        $kuyus = $this->getAllKuyuWithConnections();
        $settings= Setting::get()->first();
        $kuyus = $this->getAllKuyu();
        return view('home',compact('settings','depos','kuyus'));
    }
    public function settings(){
        return view('settings.index',compact('post'));
    }
    public function getDepo(){
        if($_GET["id"]!= ""){
            $depos = Device::Where("id",$_GET["id"])->Where("type_id",1)->Where("company_id",Auth::user()->company_id)->get()->first();
                $depos->childsize = count(DepoKuyu::select("id")->where("depo_id",$depos->id)->get()->toArray());
                $depos->deposkuyus = DepoKuyu::select("kuyu_id")->Where("depo_id",$depos->id)->get()->toArray();//depo düzenlerken bağlı kuyuları seçili getirmek için değişken
            return $depos;
        }
    }
   
    public function getSelectedKuyus($id){
        $connected = $this->connectedSerials();
        $kuyusidlist = DepoKuyu::Select("kuyu_id")->Where("depo_id",$id)->get()->toArray();
        $kuyus = Device::whereIn("id",$kuyusidlist)->get();
        foreach ($connected as $serial) {
            foreach ($kuyus as $kuyu) {
                if($kuyu->serial==$serial){
                    $kuyu->connected= true;
                }
            }
        }
        foreach($kuyus as $item){
            $item->childsize = count(DepoKuyu::select("id")->where("kuyu_id",$item->id)->get()->toArray());
        }
        return $kuyus;

    }
    public function getKuyu(){
        if($_GET["id"]!= ""){
            $kuyus = Device::Where("id",$_GET["id"])->Where("type_id",2)->Where("company_id",Auth::user()->company_id)->get()->first();
                $kuyus->childsize = count(DepoKuyu::select("id")->where("kuyu_id",$kuyus->id)->get()->toArray());
                $kuyus->kuyusdepos = DepoKuyu::select("depo_id")->Where("kuyu_id",$kuyus->id)->get()->toArray();
            return $kuyus;
        }
    }
    public function getAllKuyu(){ //mqtt için
        $kuyus= Device::Where("type_id",2)->Where("company_id",Auth::user()->company_id)->get();
        foreach ($kuyus as $item ){
            $item->connected = false;
            $item->childsize = count(DepoKuyu::select("id")->where("kuyu_id",$item->id)->get()->toArray());
        }
        return $kuyus;
    }
    public function getAllDepo(){
        $depos = Device::Where("type_id",1)->Where("company_id",Auth::user()->company_id)->get();
        foreach ($depos as $item){
            $item->childsize = count(DepoKuyu::select("id")->where("depo_id",$item->id)->get()->toArray());
            $item->connected = false;
        }
        return $depos;
    }
   
    public function connectedSerials(){ //apiden clientidleri çekme
        $settings=Setting::All();
        $username='admin';
        $password='public';
        $URL='http://'.$settings[0]->mqtt_ip.':18083/api/v3/connections/?_page=1&_limit=10000';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        $result=curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        curl_close ($ch);
        $json_data = json_decode ( $result );
        if(count($json_data->data)>0){
            $saltdata = $json_data->data;
            $serial_array = array();
            foreach ($saltdata as $item) {
                $stringcek = $item->client_id;
                $serial = explode("-",$stringcek);
                if(count($serial)>=2){ // - ye göre bölünme işlemi varsa 1.indisi al
                    array_push($serial_array,$serial[1]);
                }
            }
            // dd($serial_array);
            return $serial_array;
        }
        else{
            return [];
        }

    }
    public function getAllDepoWithConnections(){ //bağlı depoları getirme
        $connected = $this->connectedSerials();
        $depos = $this->getAllDepo();
        foreach ($connected as $serial) {
            foreach ($depos as $depo) {
                if($depo->serial==$serial){
                    $depo->connected= true;
                }
            }
        }
        return $depos;

    }
    public function getAllKuyuWithConnections(){ //bağlı kuyuları getirme
        $connected = $this->connectedSerials();
        $kuyus = $this->getAllKuyu();
        foreach ($connected as $serial) {
            foreach ($kuyus as $kuyu) {
                if($kuyu->serial==$serial){
                    $kuyu->connected= true;
                }
            }
        }
        return $kuyus;

    }
    public function getSerialsForMqtt()
    {
        $serials = Device::Select("serial")->Where("company_id",Auth::user()->company_id)->get();
        return $serials;
    }
}
