<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Device;
use App\DepoKuyu;
use App\Log;
use App\LogRecord;
use App\Settings;
use App\Company;
use App\User;


class ApiController extends Controller
{
    
    public function GetDevices(){ //seriallerini al
        $serials = Device::Select('serial','name','baslangic_saati','bitis_saati','control_time','pompa_debi','depo_height','depo_volume','depo_max_level','depo_min_level','total_working_hours','phone1','phone2','phone3','email1','email2')->get()->toArray();
        return $serials;
    }

    public function DepoKuyu(){ //
        // $response = DepoKuyu::Select('depo_id','kuyu_id')->join('devices','depokuyus.depo_id','devices.id')->join('devices','depokuyus.kuyu_id','devices.id')->get();
        // return $response;
        $relations = DepoKuyu::Select('depo_id','kuyu_id')->get();
        $devices = Device::Select('*')->get()->toArray();
        foreach($relations as $relation){
            $relation->depo = $devices[array_search($relation->depo_id,array_column($devices, 'id'))];
            $relation->kuyu = $devices[array_search($relation->kuyu_id,array_column($devices, 'id'))];
        }
        return $relations;
    }

    public function DeviceSettings(){ //Başlangıç bitiş saatlerini al
     
        $settingsid = Device::Select('serial')->get();
        foreach ($settingsid as $element =>$item) {
            $settings = Device::Select("*")->where('serial',$item["serial"])->get()->toArray();
            $settings = $settings[0];
            $new_list [$item["serial"]] = array(
                "baslangic_saati" =>$settings["baslangic_saati"],
                "bitis_saati" =>$settings["bitis_saati"],
                "control_time" =>$settings["control_time"],
                "name" =>$settings["name"],
                "pompa_debi" =>$settings["pompa_debi"],
                "depo_height" =>$settings["depo_height"],
                "depo_max_level" =>$settings["depo_max_level"],
                "depo_min_level" =>$settings["depo_min_level"],
                "total_working_hours" =>$settings["total_working_hours"],
                "phone1" =>$settings["phone1"],
                "phone2" =>$settings["phone2"],
                "phone3" =>$settings["phone3"],
                "email1" =>$settings["email1"],
                "email2" =>$settings["email2"],
            );
        }
        return $new_list;
    }

    public function Logs(){
       $logsid = Log::Select('device_id')->get();
       foreach($logsid as $element =>$item){
           $logs = Log::Select("*")->where('device_id',$item['device_id'])->get()->toArray();
           $logs = $logs[0];
           $log_list [$item["device_id"]] = array(
               "islem_deger" =>$logs["islem_deger"],
               "islem_id" =>$logs["islem_id"],
               "device_id" =>$logs["device_id"],
               "user_id" =>$logs["user_id"],
               "company_id" =>$logs["company_id"],
               'tarih' =>$logs['tarih']
           );
       }
       return $log_list;
   

    }


    


    
}
