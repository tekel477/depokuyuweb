<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Company;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       if(Auth::user()->role_id=='1'){
        $companies = Company::all();
        return view('companies.index',compact('companies'));
       }else{
           return redirect()->route('home');
       }
        
    }

    public function create(){
        $companies = new Company();
        if(Auth::user()->role_id =='1'){
            return view('companies.create');
        }else{
            return redirect('/home');
        }
        
    }

    public function store(Request $request){
        if(Auth::user()->role_id=='1'){
           $companies = new Company();
           $companies->name = $request->name;
           $companies->unvan = $request->unvan; 
           $companies->address = $request->address; 
           $companies->phone = $request->phone;
           $companies->save();
           Session::flash('statuscode','success');
           return redirect('company')->with('status','Firma Eklendi'); 
        }else{
            Session::flash('statuscode','error');
            return redirect('/company/create')->with('status','Firma Eklenemedi');
        }

    }
    public function edit($id)
    {
        $companies = Company::find($id);
        return view('companies.edit',compact('companies'));
    }

    public function update(Request $request, $id){
        if(Auth::user()->role_id=='1' || Auth::user()->role_id=='2'){
            $companies = Company::findOrFail($id);
            $companies->name = $request->name;
            $companies->unvan = $request->unvan;
            $companies->address = $request->address;
            $companies->phone = $request->phone;
            $companies->save();
            Session::flash('statuscode','success');
            return redirect('/company')->with('status','Firma Bilgileri Güncellendi');
        }else{
            return 'Yetkisiz Erişim';
        }

    }

    public function destroy($id)
    {
        if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
         $companies = Company::find($id);
         $companies->delete();
         return redirect('/company')->with('status','Firma Silindi');
        }else{
            return 'Yetkisiz Erişim';
        }
    }
}
