<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Role;
use App\Company;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Admin tüm firmalara ait kullanıcıları görebilecek
        if(Auth::user()->role_id=='1'){
            $users =User::all();
            return view('users.index',compact('users'));
        }else{

            $users =  User::Where('company_id',Auth::user()->company_id)->get();
            foreach ($users as $user) {
                $cek = $user->company;
            }
             return view('users.index',compact('users'));
        }
    }

    public function create()
    {
        $roles = Role::all();
        $users = new User();
        $companies = Company::all();
        return view('users.create',compact('roles','users','companies'));
    }

    
     public function store(Request $request)
    {
        $roles = Role::all();
        $companies = Company::all();
        $checkemail=User::select("id")->where("email",$request->email)->get()->first();
        //dd($checkemail);
            if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
                if($checkemail==null){
                    $users = new User();
                    $users->name = $request->name;
                    $users->email = $request->email;
                    $users->password = Hash::make($request->password);
                    $users->remember_token = Hash::make($request->remember_token);
                    $users->role_id = $request->role_id;
                    $users->phone = $request->phone;
                    $users->company_id = $request->company_id;
                    $users->save();
                    Session::flash('statuscode','success');
                    return redirect('/users')->with('status','Kullanıcı Eklendi');
                    
                }else{
                    //response ile yapılan kontrol mesajı
                    // $response=["status"=>"error","message"=>"Girmiş olduğunuz email adresi kullanılamaz."];
                    // return view("users.create",compact('response','roles'));
                    Session::flash('statuscode','error');
                    return redirect('/users')->with('status','Girmiş olduğunuz email adresi alınmıştır.Lütfen farklı bir email adresi giriniz.');
                }
            }else{
                Session::flash('statuscode','error');
                    return redirect('/users')->with('status','Kullanıcı Ekleme Yetkiniz Yoktur.');
            }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $users = User::find($id);
        $roles = Role::all();
        return view('users.edit',compact('users','roles'));
    }

    
    public function update(Request $request, $id)
    {
        if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
        $users = User::findOrFail($id);
        $users->name = $request->name;
        $users->email = $request->email;
        $users->role_id = $request->role_id;
        $users->phone = $request->phone;
        $users->save();
        
        Session::flash('statuscode','info');
        return redirect('/users')->with('status','Kullanıcı Güncellendi');
        }else{
            Session::flash('statuscode','error');
            return redirect('/users')->with('status','Kullanıcı Güncelleme Yetkiniz Yoktur');
        }
    }

    
    public function destroy($id)
    {
        if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
         $users = User::find($id);
         $users->delete();
         return redirect('/users')->with('status','Kullanıcı Silindi');
        }else{
            Session::flash('statuscode','error');
            return redirect('/users')->with('status','Kullanıcı Silme Yetkiniz Yoktur');
        }
    }

    public function reset_Password(Request $request,$id){
        if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
          $request->validate([
              'password'=>'required',
              'password_again' => 'required'
          ]);
          $users=User::find($id);
          if($request->password==$request->password_again){
              $users->password=Hash::make($request->password);
              $users->save();
              Session::flash('statuscode','success');
              return redirect('/users')->with('status','İşlem Başarı ile Gerçekleştirildi');
            }else {
                Session::flash('statuscode','error');
                return view('users.edit')->with('status','Yeni Şifre Eskisiyle Eşleşmiyor');
            }
            
        }else{
                return 'Yetkisiz Erişim';
            }

    }

    public function currentacount(){
        $roles=Role::all();
        $users = Auth::user(); //Auth sınıfından giriş yapıldığı için 
        return view("users.currentacount",compact('users','roles'));
    }

    public function acountupdate(Request $request, $id){
        if(Auth::id()== $id){
            $request->validate([
                'name'=>'required',
                'email'=>'required|email' 
            ]);

            $users = User::find($id);
            $users->name= $request->name;
            $users->email= $request->email;
            $users->phone= $request->phone;
            $users->save();
            Session::flash('statuscode','success');
            return redirect('/users/currentacount')->with('status','İşlem Başarı ile Gerçekleştirildi');
        }else{
            Session::flash('statuscode','error');
            return view('users.currentacount')->with('status','İşleminiz Gerçekleştirilemedi');
        }
    }

    public function change_password(Request $request,$id){
        if(Auth::id()==$id){
            $request->validate([
                'password' => 'required',
                'password_again' => 'required',
                'password_old' => 'required',
            ]);
            
            $users= User::find($id);
            if($request->password== $request->password_again){
                if(Hash::check($request->password_old,$users->password)){
                    $users->password = Hash::make($request->password);
                    $users->save();
                    Session::flash('statuscode','success');
                    return redirect('/users/currentacount')->with('status','İşlem Başarıyla Kaydedildi');
                }else{
                    Session::flash('statuscode','error');
                    return redirect('/users/currentacount')->with('status','Tekar Deneyiniz!Mevcut Şifreniz Yanlış');
                }
            }else{
                    Session::flash('statuscode','warning');
                    return redirect('/users/currentacount')->with('status','Yeni Şifreler Birbirleriyle Eşleşmiyor');
            }
        }
    }
}
