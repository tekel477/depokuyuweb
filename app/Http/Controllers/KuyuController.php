<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Device;
use App\DepoKuyu;

class KuyuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //
    }

    public function create(){
        $depos = Device::All();
        $kuyus = new Kuyu();
        return view('kuyus.create',compact('depos','kuyus'));
    }
    
    public function store(){
        if($_GET["serial"]!="" && $_GET["name"]!= ""){
            $ctrlserial=Device::select("id")->where("serial",$_GET["serial"])->get()->first();
            $ctrlname=Device::select("id")->where("name",$_GET["name"])->get()->first();
                if($ctrlserial==null){
                    $kuyu = Device::create([
                        'company_id' =>Auth::user()->company_id,
                        'serial'=>strval($_GET["serial"]),
                        'type_id'=>2,
                        'name'=>$_GET["name"],
                        'pompa_debi' =>$_GET["pompa_debi"],
                        'total_working_hours' => $_GET["total_working_hours"],
                        'baslangic_saati' => $_GET["baslangic_saati"],
                        'bitis_saati' => $_GET["bitis_saati"],
                        'control_time' => $_GET["control_time"],
                        'phone1' =>$_GET["phone1"],
                        'phone2' =>$_GET["phone2"],
                        'phone3' =>$_GET["phone3"],
                        'email1' =>$_GET["email1"],
                        'email2' =>$_GET["email2"]
                    ]);
                    return 'success';
                }else{
                    return 'Cihazların Seri Numaraları Farklı Olmalıdır';
                }
        }else{
            return 'Lütfen Tüm Boş Alanları Doldurunuz';
        }
    }
    
    public function update(Request $request, $id){
        $kuyu = Device::Select('serial')->Where('id',$id)->get();
            if($kuyu!=null){
                $kuyu->name = $request->input('name');
                $kuyu->pompa_debi = $request->input('pompa_debi');
                $kuyu->total_working_hours = $request->input('total_working_hours');
                $kuyu->baslangic_saati = $request->input('kuyu_edit_baslangic_saati');
                $kuyu->bitis_saati = $request->input('kuyu_edit_bitis_saati');
                $kuyu->control_time = $request->input('kuyu_edit_control_time');
                $kuyu->phone1 = $request->input('edit_kuyu_phone1');
                $kuyu->phone2 = $request->input('edit_kuyu_phone2');
                $kuyu->phone3 = $request->input('edit_kuyu_phone3');
                $kuyu->email1 = $request->input('edit_kuyu_email1');
                $kuyu->email2 = $request->input('edit_kuyu_email2');
                $kuyu->save();
                $response = ["status" => "success","serial"=>$kuyus[0]->serial];
                DepoKuyu::Where("kuyu_id",$kuyu->id)->delete();
                foreach($request->input('kuyusdepos') as $depoid){
                    DepoKuyu::create([
                        'kuyu_id'=>$kuyu->id,
                        'depo_id' => $depoid,
                    ]);
                }
                return $response;
            }else{
                return 'Güncellenmedi';
            }

    }

    public function destroy($id){
        if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
            $kuyus = Device::Select('serial')->Where('id',$id)->get();
            DepoKuyu::Where("kuyu_id",$id)->delete();
            Device::Where("id",$id)->delete();
            $response = ["status" => "success","serial"=>$kuyus[0]->serial];
            return $response;
        }else{
            'Yetkisiz Erişim';
        }
    
    }
}
