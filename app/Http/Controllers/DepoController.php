<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Device;
use App\DepoKuyu;


class DepoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function index()
     {
        $depos = Device::Where("type_id",1)->Where("company_id",Auth::user()->company_id)->get();
        $kuyus= Device::Where("type_id",2)->Where("company_id",Auth::user()->company_id)->get();
         return view('home',compact('depos','kuyus'));
        // // dd($kuyus);
     }
    public function create()
    {
        $kuyus = Device::All();
        return view('kuyus.create',compact('kuyus'));
    }
    public function store()
    {
            if($_GET["serial"]!="" && $_GET["name"]!= ""){
                $ctrlserial=Device::select("id")->where("serial",$_GET["serial"])->get()->first();
                $ctrlname=Device::select("id")->where("name",$_GET["name"])->get()->first();
                    if($ctrlserial==null){
                        $depos = Device::create([
                            'type_id' =>1,
                            'company_id' =>Auth::user()->company_id,
                            'serial'=>strval($_GET["serial"]),
                            'name'=>$_GET["name"],
                            'depo_volume'=>$_GET["depo_volume"],
                            'depo_height'=>$_GET["depo_height"],
                            'depo_max_level'=>$_GET["depo_max_level"],
                            'depo_min_level'=>$_GET["depo_min_level"],
                            'baslangic_saati'=>$_GET["baslangic_saati"],
                            'bitis_saati'=>$_GET["bitis_saati"],
                            'control_time'=>$_GET["control_time"],
                            'phone1' =>$_GET["phone1"],
                            'phone2' =>$_GET["phone2"],
                            'phone3' =>$_GET["phone3"],
                            'email1' =>$_GET["email1"],
                            'email2' =>$_GET["email2"],
                        ]);
                        $kuyulist = explode(",",$_GET["deposkuyus"]);
                        foreach ($kuyulist as $kuyuid){
                            DepoKuyu::create([
                                'depo_id' => $depos->id,
                                'kuyu_id' => $kuyuid,
    
                            ]);
                            return 'success';
                        }
                        
                        
                    }else{
                        return 'Cihazların Seri Numaralı Farklı Olmalıdır';
                    }
            }else{
                return 'Lütfen Tüm Boş Alanları Doldurunuz';
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $depo = Device::find($id);
            if($depo!=null){
                $depo->name = $request->input('name');
                $depo->serial = $request->input('serial');
                $depo->depo_volume = $request->input('depo_volume');
                $depo->depo_height = $request->input('depo_height');
                $depo->depo_max_level = $request->input('depo_max_level');
                $depo->depo_min_level = $request->input('depo_min_level');  
                $depo->baslangic_saati = $request->input('edit_baslangic_saati');  
                $depo->bitis_saati = $request->input('edit_bitis_saati');  
                $depo->control_time = $request->input('edit_depo_control_time');  
                $depo->phone1 = $request->input('edit_depo_phone1');  
                $depo->phone2 = $request->input('edit_depo_phone2');  
                $depo->phone3 = $request->input('edit_depo_phone3');  
                $depo->email1 = $request->input('edit_depo_email1');  
                $depo->email2 = $request->input('edit_depo_email2');  
                $depo->save();
                DepoKuyu::Where("depo_id",$depo->id)->delete();
                    foreach ($request->input('deposkuyus') as $kuyuid){
                        DepoKuyu::create([
                            'depo_id' => $depo->id,
                            'kuyu_id' => $kuyuid,
                        ]);
                    }
                    return 'success';
            }else{
                return 'Güncellenmedi';
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->role_id=='1'|| Auth::user()->role_id=='2'){
            $serials=Device::Select('serial')->Where('id',$id)->get();
            DepoKuyu::Select('id')->Where("depo_id",$id)->delete();
            Device::Where("id",$id)->delete();
            $response = ["status"=>"success","serial"=>$serials[0]->serial];
            return $response;
        }else{
            'Yetkisiz Erişim';
        }
    }
}
