<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table= 'companies';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasMany('App\User' , 'company_id');
    }

    public function Log(){
        return $this->hasMany('App\Log','company_id');
    }
    public function logg(){
        return $this->hasMany('App\Logg','company_id');
    }
    

    
}
