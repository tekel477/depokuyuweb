<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use softDeletes;
    
    protected $guarded = ["id"];

    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    

    public function role(){
        return $this->belongsTo(App\Role::class, $role_id);
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function devices(){
        return $this->hasMany('App\Device');
    }

    public function logs(){
        return $this->hasMany('App\Log');
    }
    public function logg(){
        return $this->hasMany('App\Logg');
    }


    
}