<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';
    protected $guarded = ['id'];

    public function logs(){
        return $this->hasMany('App\Log', 'device_id');
    }
    public function logg(){
        return $this->hasMany('App\Logg', 'device_id');
    }

    
    
}
