<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';
    protected $guarded = ['id'];

    public function device(){
        return $this->belongsTo('App\Device');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function company(){
        return $this->belongsTo('App\Company');
    }
    public function logrecord(){
        return $this->belongsTo('App\LogRecord','islem_id');
    }
}
