<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logg extends Model
{
    protected $table = 'logg';
    protected $guarded = ['id'];

    public function device(){
        return $this->belongsTo('App\Device');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function company(){
        return $this->belongsTo('App\Company');
    }
}
