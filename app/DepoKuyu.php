<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepoKuyu extends Model
{
    protected $dates = ['deleted_at'];
    protected $guarded = ['id'];

    public function Depo(){
        return $this->belongsTo('App\Device', 'depo_id');
    }
    public function Kuyu(){
        return $this->belongsTo('App\Device', 'kuyu_id');
    }

}


