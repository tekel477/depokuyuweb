<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('/getdevices','ApiController@GetDevices');
Route::get('/depokuyu','ApiController@DepoKuyu');
Route::get('/settings','ApiController@DeviceSettings');
Route::get('/logs','ApiController@Logs'); 

//settings ayarlar için diğer yol $list [] = array(
            //     $item->serial=>$settings
            // );