<?php

use Illuminate\Support\Facades\Route;
use App\Depo;
use App\Kuyu;
use App\DepoKuyu;


Route::get('/','HomeController@index')->name("welcome");
Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');

//   user işlemleri
Route::get('/users','UserController@index')->name('users.index');
Route::get('/users/create','UserController@create')->name('users.create');
Route::post('/users/store','UserController@store')->name('users.store');
Route::get('/users/edit/{id}','UserController@edit')->name('users.edit');
Route::patch('/users/update/{id}','UserController@update')->name('users.update');
Route::POST('/user/destroy/{id}','UserController@destroy')->name('user.destroy');
Route::patch('/users/resetPassword/{id}','UserController@reset_Password')->name('users.resetPassword');
Route::get('/users/currentacount', 'UserController@currentacount')->name('users.currentacount');
Route::POST('/users/{id}/acountupdate','UserController@acountUpdate')->name('users.acountupdate');
Route::patch('/users/changepassword/{id}','UserController@change_password')->name('users.changepassword');

// DepoController işlemleri
 Route::get('/depo/store','DepoController@store')->name('depo.store');
 Route::patch('/depo/update/{id}','DepoController@update')->name('depo.update');
 Route::get('/depo/destroy/{id}','DepoController@destroy')->name('depo.destroy');

 //KuyuController işlemleri
 Route::get('/kuyu/store','KuyuController@store')->name('kuyu.store');
 Route::post('/kuyu/update/{id}','KuyuController@update')->name('kuyu.update');
 Route::get('/kuyu/destroy/{id}','KuyuController@destroy')->name('kuyu.destroy');
 //setting sayfası
Route::get('/settings','SettingController@index')->name('settings');
Route::get('/settings/update/{id}','SettingController@update')->name('settings.update');
//HomeController-mqtt ayarları
Route::get('/home/getdepo','HomeController@getDepo')->name('depo.getdepo');
Route::get('/home/getkuyu','HomeController@getKuyu')->name('kuyu.getkuyu');
Route::get('/home/getallkuyu','HomeController@getAllKuyu')->name('kuyu.getallkuyu');
Route::get('/home/getalldepo','HomeController@getAllDepo')->name('depo.getalldepo');
Route::get('/home/getserialsformqtt','HomeController@getSerialsForMqtt')->name('depo.getSerialsForMqtt');
Route::get('/home/getselectedkuyus/{id}','HomeController@getSelectedKuyus')->name('depo.getselectedkuyus');
Route::get('/home/getalldepowithconnections','HomeController@getAllDepoWithConnections')->name('depo.getalldepowithconnections');
//CompanyController
Route::get('/company','CompanyController@index')->name('companies.index');
Route::get('/company/create','CompanyController@create')->name('companies.create');
Route::get('/company/edit/{id}','CompanyController@edit')->name('companies.edit');
Route::post('company/store','CompanyController@store')->name('companies.store');
Route::patch('/company/update/{id}','CompanyController@update')->name('companies.update');
Route::POST('/company/destroy/{id}','CompanyController@destroy')->name('companies.destroy');

//Log Records

Route::get('/logs','LogController@index')->name('logs.index');
// Route::get('/logsrecords','LogController@index')->name('logrecords.index');
// Route::get('/logs/DeviceControlLog','LogController@Logg')->name('logs.DeviceControlLog');
