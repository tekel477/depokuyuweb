<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoggTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('command');
            $table->bigInteger('device_id');
            $table->bigInteger('company_id');
            $table->bigInteger('user_id')->nullable();
            $table->datetime('tarih');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logg');
    }
}
