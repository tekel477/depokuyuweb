<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('serial',50);
            $table->integer('company_id');
            $table->integer('type_id');
            $table->integer('pompa_debi')->nullable();
            $table->integer('depo_height')->nullable();
            $table->integer('depo_volume')->nullable();
            $table->integer('depo_max_level')->nullable();
            $table->integer('depo_min_level')->nullable();
            $table->integer('total_working_hours')->nullable();
            $table->string('baslangic_saati');
            $table->string('bitis_saati');
            $table->string('control_time');
            $table->string('phone1');
            $table->string('phone2');
            $table->string('phone3');
            $table->string('email1');
            $table->string('email2');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
