-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 12 Kas 2020, 13:35:27
-- Sunucu sürümü: 10.4.14-MariaDB
-- PHP Sürümü: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `depokuyu`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unvan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `companies`
--

INSERT INTO `companies` (`id`, `name`, `unvan`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'ArgeRF', 'Arge', 'Kayseri/Türkiye', '5433410434', '2020-10-28 10:33:28', '2020-10-28 10:33:28'),
(2, 'Yazılım', 'YazılımArge', 'Kayseri/Türkiye', '0543625698', '2020-10-28 10:38:57', '2020-10-28 10:38:57'),
(3, 'Firma3', 'Firma', 'Kayseri/Türkiye', '5433410434', '2020-11-07 06:05:45', '2020-11-07 06:05:45');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `depo_kuyus`
--

CREATE TABLE `depo_kuyus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `depo_id` int(11) NOT NULL,
  `kuyu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `depo_kuyus`
--

INSERT INTO `depo_kuyus` (`id`, `depo_id`, `kuyu_id`, `created_at`, `updated_at`) VALUES
(220, 14, 6, '2020-11-06 04:05:54', '2020-11-06 04:05:54'),
(222, 3, 6, '2020-11-06 07:03:01', '2020-11-06 07:03:01'),
(225, 9, 6, '2020-11-06 07:03:19', '2020-11-06 07:03:19'),
(228, 7, 6, '2020-11-06 07:16:17', '2020-11-06 07:16:17'),
(230, 20, 19, '2020-11-06 08:50:33', '2020-11-06 08:50:33'),
(233, 7, 18, '2020-11-09 03:07:55', '2020-11-09 03:07:55'),
(234, 17, 18, '2020-11-09 03:07:55', '2020-11-09 03:07:55'),
(236, 9, 16, '2020-11-09 03:18:02', '2020-11-09 03:18:02'),
(237, 15, 16, '2020-11-09 03:18:02', '2020-11-09 03:18:02'),
(245, 1, 6, '2020-11-09 03:36:20', '2020-11-09 03:36:20'),
(246, 1, 2, '2020-11-09 03:47:01', '2020-11-09 03:47:01'),
(247, 3, 2, '2020-11-09 03:47:01', '2020-11-09 03:47:01'),
(248, 4, 2, '2020-11-09 03:47:01', '2020-11-09 03:47:01'),
(253, 21, 18, '2020-11-09 03:49:24', '2020-11-09 03:49:24'),
(256, 21, 24, '2020-11-09 03:49:24', '2020-11-09 03:49:24'),
(257, 21, 22, '2020-11-09 11:48:07', '2020-11-09 11:48:07'),
(258, 21, 23, '2020-11-09 11:48:16', '2020-11-09 11:48:16'),
(259, 25, 23, '2020-11-11 04:16:41', '2020-11-11 04:16:41');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `devices`
--

CREATE TABLE `devices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `pompa_debi` int(11) DEFAULT NULL,
  `depo_height` int(11) DEFAULT NULL,
  `depo_volume` int(11) DEFAULT NULL,
  `depo_max_level` int(11) DEFAULT NULL,
  `depo_min_level` int(11) DEFAULT NULL,
  `total_working_hours` int(11) DEFAULT NULL,
  `baslangic_saati` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bitis_saati` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `control_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `devices`
--

INSERT INTO `devices` (`id`, `name`, `serial`, `company_id`, `type_id`, `pompa_debi`, `depo_height`, `depo_volume`, `depo_max_level`, `depo_min_level`, `total_working_hours`, `baslangic_saati`, `bitis_saati`, `control_time`, `deleted_at`, `created_at`, `updated_at`, `last_time`) VALUES
(1, 'Depo1', '18683250298120071', 1, 1, NULL, 400, 305, 150, 50, NULL, '08.30', '18.30', '30', NULL, '2020-10-28 05:01:46', '2020-11-09 03:36:20', ''),
(2, 'Kuyu1', '18683250288128471', 1, 2, 200, NULL, NULL, NULL, NULL, 4, '08.30', '18.30', '20', NULL, '2020-10-28 05:03:14', '2020-11-09 03:47:01', ''),
(3, 'Depo2', '18683250298120271', 1, 1, NULL, 300, 405, 200, 80, NULL, '', '', '', NULL, '2020-10-28 05:03:57', '2020-11-06 04:03:01', ''),
(4, 'Depo3', '18683250298130171', 1, 1, NULL, 250, 500, 100, 25, NULL, '', '', '', NULL, '2020-10-28 05:04:27', '2020-10-28 05:04:27', ''),
(6, 'Kuyu2', '18683250288128472', 1, 2, 100, NULL, NULL, NULL, NULL, 4, '', '', '', NULL, '2020-11-03 00:50:24', '2020-11-03 00:50:24', ''),
(7, 'Depo4', '18683250298120371', 1, 1, NULL, 200, 400, 100, 50, NULL, '', '', '', NULL, '2020-11-03 02:39:42', '2020-11-06 04:16:17', ''),
(9, 'Depo5', '18683250298120471', 1, 1, NULL, 100, 300, 150, 52, NULL, '', '', '', NULL, '2020-11-03 02:50:41', '2020-11-06 04:03:19', ''),
(14, 'Depo6', '18683250298120671', 1, 1, NULL, 200, 305, 100, 20, NULL, '', '', '', NULL, '2020-11-03 05:36:39', '2020-11-06 01:05:54', ''),
(15, 'Depo7', '18683250298120771', 1, 1, NULL, 200, 300, 100, 20, NULL, '', '', '', NULL, '2020-11-03 05:37:02', '2020-11-03 05:37:02', ''),
(16, 'Kuyu3', '18683250288128473', 1, 2, 100, NULL, NULL, NULL, NULL, 6, '08.30', '18.30', '30', NULL, '2020-11-03 06:39:33', '2020-11-09 03:18:02', ''),
(17, 'Depo8', '18683250298120871', 1, 1, NULL, 300, 400, 150, 20, NULL, '', '', '', NULL, '2020-11-05 07:16:49', '2020-11-05 07:16:49', ''),
(18, 'Kuyu4', '18683250288128474', 1, 2, 200, NULL, NULL, NULL, NULL, 5, '08.30', '18.30', '30', NULL, '2020-11-05 07:17:23', '2020-11-09 03:07:55', ''),
(19, 'Kuyu1', '18683250288128477', 2, 2, 300, NULL, NULL, NULL, NULL, 5, '', '', '', NULL, '2020-11-06 05:48:24', '2020-11-06 05:48:24', ''),
(20, 'Depo1', '18683250298120879', 2, 1, NULL, 200, 300, 100, 50, NULL, '', '', '', NULL, '2020-11-06 05:50:33', '2020-11-06 05:50:33', ''),
(21, 'Depo11', '1135503589745', 1, 1, NULL, 300, 500, 150, 50, NULL, '08.30', '18.30', '20', NULL, '2020-11-07 05:14:42', '2020-11-09 03:31:07', ''),
(22, 'Kuyu5', '64587845416441', 1, 2, 100, NULL, NULL, NULL, NULL, 5, '08.30', '18.30', '30', NULL, '2020-11-09 03:09:00', '2020-11-09 11:48:07', ''),
(23, 'Kuyu6', '1862569852000', 1, 2, 100, NULL, NULL, NULL, NULL, 4, '08.30', '19.40', '20', NULL, '2020-11-09 03:45:46', '2020-11-09 11:48:16', ''),
(24, 'Kuyu7', '65200036696', 1, 2, 100, NULL, NULL, NULL, NULL, 5, '08.30', '17.30', '50', NULL, '2020-11-09 03:48:07', '2020-11-09 03:49:09', '');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `device_types`
--

CREATE TABLE `device_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `logg`
--

CREATE TABLE `logg` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `command` smallint(6) NOT NULL,
  `device_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `tarih` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `logg`
--

INSERT INTO `logg` (`id`, `command`, `device_id`, `company_id`, `user_id`, `tarih`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '2020-11-06 17:05:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `logs`
--

CREATE TABLE `logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `islem_deger` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `islem_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tarih` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `logs`
--

INSERT INTO `logs` (`id`, `islem_deger`, `islem_id`, `device_id`, `user_id`, `tarih`, `created_at`, `updated_at`, `company_id`) VALUES
(1, 'Açma', 1, 1, 1, '2020-11-06 15:39:41', '2020-11-06 12:39:41', '2020-11-06 12:39:41', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `log_records`
--

CREATE TABLE `log_records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `islem` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `log_records`
--

INSERT INTO `log_records` (`id`, `islem`, `created_at`, `updated_at`) VALUES
(1, 'Açma', '2020-11-06 12:23:16', '2020-11-06 12:23:16'),
(2, 'Kapatma', '2020-11-06 12:23:16', '2020-11-06 12:23:16');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_20_082337_create_roles_table', 1),
(5, '2020_09_10_082734_create_settings_table', 1),
(6, '2020_10_13_120425_create_companies_table', 1),
(7, '2020_10_13_121335_create_devices_table', 1),
(8, '2020_10_13_122729_create_depo_kuyus_table', 1),
(9, '2020_10_13_123641_create_device_types_table', 1),
(10, '2020_10_13_123808_create_logs_table', 1),
(11, '2020_10_13_124814_create_log_records_table', 1),
(12, '2020_10_26_201555_update_column_logs_table', 1),
(13, '2020_10_30_064238_add_column_to_device_table', 2),
(14, '2020_11_06_130553_create_logg_table', 3),
(15, '2020_11_07_074810_create_devices_table', 4),
(16, '2020_11_07_085353_create_users_table', 5),
(17, '2020_11_12_081058_add_cplımn_devices_table', 6);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `roles`
--

INSERT INTO `roles` (`id`, `role_status`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-10-28 10:33:55', '2020-10-28 10:33:55'),
(2, 2, '2020-10-28 10:33:55', '2020-10-28 10:33:55'),
(3, 3, '2020-10-28 10:34:15', '2020-10-28 10:34:15'),
(4, 4, '2020-10-28 10:34:15', '2020-10-28 10:34:15');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mqtt_ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mqtt_port` int(10) UNSIGNED NOT NULL DEFAULT 8083,
  `mqtt_user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DepoKuyuWeb',
  `mqtt_user_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'depokuyuweb',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `settings`
--

INSERT INTO `settings` (`id`, `mqtt_ip`, `mqtt_port`, `mqtt_user_name`, `mqtt_user_password`, `created_at`, `updated_at`) VALUES
(1, '192.168.1.2', 8083, 'DepoKuyuWeb', 'depokuyuweb', '2020-10-28 11:00:14', '2020-11-03 05:40:36');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `email_verified_at`, `password`, `role_id`, `company_id`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@arge.com', '5433410434', NULL, '$2y$10$zXiHFMdxnvlHPMBLqF2gJ.T9/zM9fDod9Ewpf6RkF8d4RL.lZIC.m', 1, 1, NULL, NULL, '2020-11-07 09:00:42', '2020-11-07 09:00:42'),
(2, 'Firma3kullanıcı', 'firma3kullanici@arge.com', '5433410434', NULL, '$2y$10$brI4iS2AE9zZECBgO7XVFuAN7bmX/4tzPX7V8Sx/8qAwHmK1YlVCq', 2, 3, NULL, '6nOJbQ20pq6KM0FLuO91YKwrpUywRTOTLxGvrtt4Q1te3d0UnRS9QrvaGoKF', '2020-11-07 06:10:32', '2020-11-07 06:13:02');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `depo_kuyus`
--
ALTER TABLE `depo_kuyus`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `device_types`
--
ALTER TABLE `device_types`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `logg`
--
ALTER TABLE `logg`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `log_records`
--
ALTER TABLE `log_records`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `depo_kuyus`
--
ALTER TABLE `depo_kuyus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- Tablo için AUTO_INCREMENT değeri `devices`
--
ALTER TABLE `devices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Tablo için AUTO_INCREMENT değeri `device_types`
--
ALTER TABLE `device_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `logg`
--
ALTER TABLE `logg`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `logs`
--
ALTER TABLE `logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `log_records`
--
ALTER TABLE `log_records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Tablo için AUTO_INCREMENT değeri `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
